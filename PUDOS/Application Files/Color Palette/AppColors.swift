//
//  AppColor.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

struct AppColor {
    
      static let main = AppColor()
      private init(){}
    
    // pudos color
      let pudosMainColor = #colorLiteral(red: 0, green: 0.4053570926, blue: 0.8879464269, alpha: 1)
      let pudosSecondaryColor = #colorLiteral(red: 0.8424451947, green: 0.7336782813, blue: 0.3346539438, alpha: 1)
    
    // Palette
      let blue = #colorLiteral(red: 0, green: 0.4053570926, blue: 0.8879464269, alpha: 1)
      let blueLight = #colorLiteral(red: 0.3289328516, green: 0.781253159, blue: 0.9897740483, alpha: 1)
      let yellow = #colorLiteral(red: 1, green: 0.8057073951, blue: 0, alpha: 1)
      let orange = #colorLiteral(red: 1, green: 0.5868047476, blue: 0, alpha: 1)
      let red = #colorLiteral(red: 1, green: 0.1564880013, blue: 0.3171166778, alpha: 1)
      let darkRed = #colorLiteral(red: 1, green: 0.2213764787, blue: 0.1414284706, alpha: 1)
      let green = #colorLiteral(red: 0.2658407092, green: 0.858083725, blue: 0.3677983284, alpha: 1)
    
    // Basic Grey Shade
    
      let headBodySubhead = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
      let secondarytextLink = #colorLiteral(red: 0.3999532461, green: 0.4000268579, blue: 0.3999486566, alpha: 1)
      let footnoteCaptions = #colorLiteral(red: 0.5410533547, green: 0.5408385992, blue: 0.5615035892, alpha: 1)
      let contentSectionDivider = #colorLiteral(red: 0.7842705846, green: 0.7800551057, blue: 0.8006526828, alpha: 1)
      let background = #colorLiteral(red: 0.9370916486, green: 0.9369438291, blue: 0.9575446248, alpha: 1)
      let tabbarToolbar = #colorLiteral(red: 0.9763647914, green: 0.9765316844, blue: 0.9763541818, alpha: 1)
    
}

