//
//  StubManager.swift
//  PUDOS
//
//  Created by Majid Inc on 02/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation


struct Stub {
    static func data(from resource: String, withExtension: String) -> Data{
        guard let url = Bundle.main.url(forResource: resource, withExtension: withExtension)
            else { return Data()}
        return try! Data(contentsOf: url, options: .alwaysMapped)
    }
}
