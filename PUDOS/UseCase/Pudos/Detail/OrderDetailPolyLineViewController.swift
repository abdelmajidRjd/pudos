//
//  OrderDetailPolyLineViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 10/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import MapKit
import SwiftyJSON

class OrderDetailPolyLineViewController: UIViewController {

    var orderId: String?
    private var direction: String?
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var pudosPinView: FatPinView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let orderId = orderId else {
            show(error: "Ooops")
            return
        }
        self.title = orderId
        
        self.showSpinner()
        PudosService.default.orderDetail(id: orderId ).then { (response) -> Void in
            let result = response.result
            
            if result.isSuccessful {
                
                DispatchQueue.main.async {
                    
                    guard let direction = response.details?.directions else {
                        self.show(error: "You order is cancelled or closed !")
                        return
                        
                    }
                    self.direction = direction
                    let parser  = DetailParser(direction: direction)
                    
                    self.update(parser: parser)
                    
                    
                }

            } else if result.noAuth {
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                
                self.show(error: response.message ?? "Error")
                
            }
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
    }
    
    

}


extension OrderDetailPolyLineViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        pudosPinView.up()
        }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        pudosPinView.down()
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = AppColor.main.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseIdentifier = "pin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        guard let pudosAnnotation = annotation as? PudosAnnotation else {
            return nil
        }
        
        if #available(iOS 11.0, *) {
            
            let marker = MKMarkerAnnotationView(annotation: pudosAnnotation, reuseIdentifier: reuseIdentifier)
            if pudosAnnotation.isSourceAddress {
                marker.glyphImage = #imageLiteral(resourceName: "ic_location_on_36pt")
                marker.markerTintColor = #colorLiteral(red: 0.2489523208, green: 0.2609169371, blue: 0.4912123197, alpha: 1)
                
            } else {
                marker.glyphImage = #imageLiteral(resourceName: "ic_flag")
                marker.markerTintColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            }
            marker.subtitleVisibility = .visible
            marker.titleVisibility = .visible
            
            marker.isDraggable = true
            marker.canShowCallout = true
            
            return marker
            
            
            
        } else {
            
            let pin = MKPinAnnotationView(annotation: pudosAnnotation, reuseIdentifier: reuseIdentifier)
            if pudosAnnotation.isSourceAddress {
                pin.pinTintColor = #colorLiteral(red: 0.2489523208, green: 0.2609169371, blue: 0.4912123197, alpha: 1)
            } else {
                pin.pinTintColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            }
            
            
            pin.isDraggable = true
            pin.canShowCallout = true
            
            
            return pin
        }
    }
    
    
    
    
}

extension OrderDetailPolyLineViewController {
    
    func drawPolyLine(_ orderDirections: [OrderDirection], index: Int = 1) {
        
        
        var have1Source: Bool = true
        
        let stop = orderDirections.count
        
        
        if index > stop {
            return
        }
        
        if index == stop {
            have1Source = false
        }
        
        let direction = orderDirections[index - 1]
        
        let sourceCoordinates = CLLocationCoordinate2DMake(direction.start.lat,direction.start.lng)
        let destinationCoordinates = CLLocationCoordinate2DMake(direction.end.lat,direction.end.lng)
            
        let sourceAnnotation = PudosAnnotation(title: "From", subTitle: "You order source #\(index)", coordinate: sourceCoordinates, source: true)
            
        let destinationAnnotation = PudosAnnotation(title: "To", subTitle: "You order destination", coordinate: destinationCoordinates, source: false)
            
            
            
            
            let sourcePlaceMark = MKPlacemark(coordinate: sourceCoordinates)
            let destPlaceMark = MKPlacemark(coordinate: destinationCoordinates)
            
            let sourceItem = MKMapItem(placemark: sourcePlaceMark)
            let destItem = MKMapItem(placemark: destPlaceMark)
            
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceItem
            directionRequest.destination = destItem
            directionRequest.transportType = .automobile
            directionRequest.requestsAlternateRoutes = true
            
            
            
            let directions = MKDirections(request: directionRequest)
            
            
            
            
            directions.calculate { (response, error) in
                
                guard let response = response else {
                    if let error = error {
                        print(error)
                    }
                    return
                }
                
                let route = response.routes[0]
                self.mapView.add(route.polyline, level: .aboveRoads)
                
                if have1Source {
                    
                self.mapView.addAnnotation(sourceAnnotation)
                    
                } else {
                self.mapView.addAnnotation(sourceAnnotation)
                self.mapView.addAnnotation(destinationAnnotation)
                }
                
                
                
                
                
                let rekt = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegionForMapRect(rekt), animated: true)
                have1Source = false
                
                self.drawPolyLine(orderDirections, index: index + 1)
            
        }
            
    }
    
    func update(parser: DetailParser){
        
       
        let orderDirections = parser.directions()
        
        
        
        drawPolyLine(orderDirections)
    
    }
    
    
    struct DetailParser {
        
        let direction: String
        
        
        func directions() -> ([OrderDirection]) {
            
            var coordinates:[OrderDirection] = []
            let legs = JSON(parseJSON: direction)["routes"][0]["legs"]
            
            for index in 0..<legs.count {
                
                let startLat = legs[index]["start_location"]["lat"].doubleValue
                let startLng = legs[index]["start_location"]["lng"].doubleValue
                
                let endLat = legs[index]["end_location"]["lat"].doubleValue
                let endLng = legs[index]["end_location"]["lng"].doubleValue
                
                let startLocation = Location(lat: startLat, lng: startLng)
                let endLocation = Location(lat: endLat, lng: endLng)
                
                let orderDirection = (start: startLocation, end: endLocation)
                
                coordinates.append(orderDirection)
                
            }
            
            return coordinates
            
            
            
        }
    
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {return}
        switch identifier {
        case "detailSegue":
            let destination = segue.destination as! OrderDetailTableViewController
            guard let orderId = self.orderId else { return }
            destination.orderId = orderId
        default:
            print("")
        }
        
    }
    
    
}
// Alias
typealias OrderDirection = (start: Location, end: Location)

