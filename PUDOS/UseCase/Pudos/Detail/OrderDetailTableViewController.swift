//
//  OrderDetailTableViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 10/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderDetailTableViewController: UITableViewController {

    var orderId: String?
    
    @IBOutlet weak var assignToLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var reimboursementLabel: UILabel!
    @IBOutlet weak var reimboursementMethodLabel: UILabel!
    
    @IBOutlet weak var transactionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        guard   let orderId = orderId else {
            show(error: "Ooops")
            return
        }
        
        PudosService.default.orderDetail(id: orderId ).then { (response) -> Void in
            let result = response.result
            
            /*
 
 
             "result": "success",
             "details": {
             "order_id": "J0LG4E",
             "rider_id": "6",
             "instructions": "",
             "delivery_speed": "Within 6 hours",
             "directions": "
             "distance": 638,
             "tolls": null,
             "reimb_amount": null,
             "reimb_confirm": false,
             "created": {
             "date": "2018-02-10 02:07:39.000000",
             "timezone_type": 3,
             "timezone": "America/Los_Angeles"
             },
             "modified": {
             "date": "2018-02-10 02:07:41.000000",
             "timezone_type": 3,
             "timezone": "America/Los_Angeles"
             },
             "status": 1
 
                */
            
            
            if result.isSuccessful {
                guard let detail = response.details else {return}
                self.speedLabel.text = detail.deliverySpeed
                self.assignToLabel.text = "-"
                self.reimboursementLabel.text = detail.reimbAmount
                self.reimboursementMethodLabel.text = "Cash"
                self.statusLabel.text = detail.status ? "Open" : "Closed"
                self.dateLabel.text = detail.created.date
                self.transactionLabel.text = detail.reimbAmount ?? "-" + "$"
                
                
                
            } else if result.noAuth {
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                
                self.show(error: response.message ?? "Error")
                
            }
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
    }
        

    

    @IBAction func didTapCancelButton(_ sender: Any) {
        
        
        guard  let orderId = orderId else {
            show(error: "Ooops")
            return
        }
        
        
        self.showAlert(message: "Do you want to cancel the order") {
            self.showSpinner()
            PudosService.default.cancelOrder(id: orderId).then{ (response) -> Void in
                let result = response.result
                
                if result.isSuccessful {
                    RootController.default.home()
                    
                } else {
                    
                }
                }.always {
                    self.hideSpinner()
                }.catch(execute: { (error) in
                    self.show(error: error)
                })
        }
        
        
        
        
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension OrderDetailTableViewController {
    
    func showAlert(message: String, completion:@escaping () -> ()) {
        
        let alertController = UIAlertController(title: "Cancel Order", message: message, preferredStyle: .alert)
        
        let doAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            
            completion()
        }
        let backAction = UIAlertAction(title: "Back", style: .destructive) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        
            alertController.addAction(backAction)
        
        
            alertController.addAction(doAction)
        
        
      
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    
}

 
