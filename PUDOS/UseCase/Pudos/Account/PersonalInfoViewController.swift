//
//  PersonalInfoViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 31/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage



class PersonalInfoViewController: UIViewController {
    
    
    // MARK: -  Segues
    private let sendPictureSegue = "sendPicture"
    
    @IBOutlet weak var profileImageView: CercleImage!
    
    
    @IBOutlet weak var firstNameTF: PudosTextField!
    
    @IBOutlet weak var lastNameTF: PudosTextField!
    
    @IBOutlet weak var emailTF: PudosTextField!
    // birth date
    
    @IBOutlet weak var dayTF: UITextField!
    @IBOutlet weak var monthTF: UITextField!
    @IBOutlet weak var yearTF: UITextField!
    
    @IBOutlet weak var mobileTF: PudosTextField!
    
    @IBOutlet weak var inviteTF: PudosTextField!
    
    private let keyboardManager = KeyboardHideManager()
    private let dropDown = DropDown()
    private var image: Data?
    
    private var  imagePickerController : UIImagePickerController  = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePickerController.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadPhoto))
        self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
        
        showSpinner()
        
        keyboardManager.targets = [view]
        self.monthTF.delegate = self
        
        dropDown.anchorView = monthTF
        dropDown.dataSource = Helper.instance.month
        
        dropDown.selectionAction = {  (index: Int, item: String) in
            
            self.monthTF.text = String(index - 1)
            self.dropDown.hide()
            
        }
        
        checkProfile()

        let url = UserRepository.default.getPicUrl()
        
        self.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "splash"))
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        let url = UserRepository.default.getPicUrl()
//        
//        self.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "splash"))
//        
//        super.viewWillAppear(animated)
//    }
    
    @objc func uploadPhoto(){
        
        imagePickerController.sourceType = .photoLibrary
        
        self.present(imagePickerController, animated: true)
    }
    
    fileprivate func checkProfile(){
        
        PudosService.default.profile().then { (response) -> Void in
            
            self.hideSpinner()
            
            let result = response.result
            
            if result.isSuccessful {
                
                self.updateView(user: response.profile)
                
            } else if result.noAuth {
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
            } else {
                
                print(response.message ?? "Failed")
                let error = NSError(domain: "NSServerError", code: -1)
                self.show(error: error)
            }
            
            }.always {
                
                self.hideSpinner()
                
            }.catch { (error) in
                
                self.show(error: error)
        }
        
    }
    
    
    fileprivate func updateView(user: User?){
        
        guard let user = user else {
            
            let error = NSError(domain: "ProfileNetworkError", code: 0, userInfo: nil)
            self.show(error: error)
            return
        }
        let dateParser = DateParser(date: user.birthDate.date)
        let date = dateParser.formattedDate()
       
        self.firstNameTF.text = user.firstName
        self.lastNameTF.text = user.lastName
        self.mobileTF.text = user.mobile
        self.emailTF.text = user.email
 
       
        
        
        self.inviteTF.text = user.referralCode
        self.dayTF.text = dateParser.day()
        self.monthTF.text = dateParser.month()
        self.yearTF.text = dateParser.year()
        
        
        UserRepository
            .default
            .save(firstname: user.firstName.noSpace, lastname: user.lastName.noSpace, email: user.email.noSpace, birthDate: date, mobile: user.mobile, invite: user.referralCode.noSpace, image: image)
        
        
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        
        guard let firstName = firstNameTF.text else {return}
        guard let lastName = lastNameTF.text else {return}
        guard let mobile = mobileTF.text else {return}
        guard let invite = inviteTF.text else {return}
        
        
        guard let day = self.dayTF.text else {return}
        guard let month = self.monthTF.text else {return}
        guard let year = self.yearTF.text else {return}
        
        let bithdate = [year,month,day].joined(separator: "-")
        
        
        
        PudosService.default.updateProfile(firstName: firstName, lastName: lastName, mobile: mobile, birthDate: bithdate, oldPassword: "", newPassword: "", confirmPassword: "", file: self.image, pictureAction: "", invite: invite).then { (response) -> Void in
            
            let result = response.result
            if result.isSuccessful {
                
                self.profileImageView.sd_setImage(with: URL(string: response.pic!), placeholderImage: UIImage(named: "splash"))
                
                UserRepository.default.save(pic: response.pic!)
                
            } else if result.noAuth {
                
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                self.show(error: response.message!)
            }
  
        }
        
    }
    
    @IBAction func didTapConsultPic(_ sender: Any) {
        if let _ = profileImageView.image  {
            self.performSegue(withIdentifier: sendPictureSegue, sender: nil)
        }
    }
    
    @IBAction func didTapTrashButton(_ sender: Any) {
        
      
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identider = segue.identifier else { return }
        switch identider {
        case sendPictureSegue:
            
            guard let image = self.profileImageView.image else {
                return
            }
            let destination = segue.destination as! PhotoAccountViewController
            
            
            destination.image = image
            
        
            
        
        default:
            print("")
        }
        
        
    }
    
    
    
    
    
}
extension PersonalInfoViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        dropDown.show()
        
    }
    
}

extension PersonalInfoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.image = UIImagePNGRepresentation(image)
        
        self.profileImageView.image = image
        
        
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
  
    

