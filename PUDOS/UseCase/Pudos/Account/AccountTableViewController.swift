//
//  AccountTableViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 05/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SDWebImage


class AccountTableViewController: UITableViewController {

    @IBOutlet weak var accountPicture: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let url = UserRepository.default.getPicUrl()
        self.accountPicture.sd_setImage(with: url, placeholderImage: UIImage(named: "splash"))
        super.viewWillAppear(animated)
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let row = indexPath.row
         let section = indexPath.section
        
        switch (row,section) {
        case (0,2):
            // Invitation
           // share(message: "Pudos -- Your Next Gene App")
            invite()
        case (0,3):
            SessionManager.sharedInstance.invalidate()
            RootController.default.initialeViewController()
        default:
            return
        }
        
    }
    
    private func share(message: String){
        
        let textToShare = [ message ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    private func invite() {
        
        let alertConroller = UIAlertController(title: "invite", message: "Enter your friend email", preferredStyle: .alert)
        
        
        alertConroller.addTextField { (textField) in
            textField.placeholder = "Enter email"
            textField.textColor = AppColor.main.pudosMainColor
            textField.clearButtonMode = .whileEditing
            textField.borderStyle = .none
            textField.backgroundColor = UIColor.clear
        }
        
        
        let inviteAction = UIAlertAction(title: "Send", style: .default) { (action) in
            if let email = alertConroller.textFields![0].text {
                
                if !email.isEmpty {
                    PudosService
                        .default
                        .sendInviteEmail(email: email).then(execute: { (response) -> Void in
                            
                            if response.result.isSuccessful {
                                
                            } else {
                                self.show(error: response.message ?? "Error")
                            }

                    })
                } else {
                    self.show(error: "Please enter a valid email")
                    
                }
                
            }
        }
        let dismissAction = UIAlertAction(title: "Cancel", style: .default) { _ in
            self.dismiss(animated: true)
        }
            alertConroller.addAction(inviteAction)
            alertConroller.addAction(dismissAction)
            
            self.present(alertConroller, animated: true)    
            
        }
 
    
    
        
        
        
        
    }
    
    
    

