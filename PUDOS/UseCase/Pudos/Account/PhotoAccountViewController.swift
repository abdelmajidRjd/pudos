//
//  PhotoAccountViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 06/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PhotoAccountViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    var image : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.scrollView.minimumZoomScale = 1
        self.scrollView.maximumZoomScale = 6.0
        
        if let image = image {
            
            self.imageView.image = image
        }
        
        let recognizer = UISwipeGestureRecognizer(target: self, action: #selector(dismissView))
        recognizer.direction = .down
        scrollView.addGestureRecognizer(recognizer)
        
        
    }
    @objc func dismissView(){
        
        self.dismiss(animated: true)
    }
    
}
extension PhotoAccountViewController : UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
