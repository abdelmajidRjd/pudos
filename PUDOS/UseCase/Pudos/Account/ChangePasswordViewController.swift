//
//  ChangePasswordViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    
    @IBOutlet weak var oldPasswordTF: PudosTextField!
    
    @IBOutlet weak var newPasswordTF: PudosTextField!
    
    @IBOutlet weak var confirmPasswordTF: PudosTextField!
    
    @IBOutlet weak var contentView: UIStackView!
    private let keyboardManager = KeyboardHideManager()
    
    private var alertManager : AlertViewHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        keyboardManager.targets = [view]
        alertManager = AlertViewHandler(contentStackView: contentView)
        
        
    }
    
    
   
    
    @IBAction func didSaveButtonWasPressed(_ sender: Any) {
        
        guard let user = UserRepository.default.getUser() else { return }
        
        guard let old = oldPasswordTF.text,
             let new = newPasswordTF.text,
             let confirm = confirmPasswordTF.text else {return}
        
        
        let action = ProfileAction.default.rawValue
        
        self.showSpinner()
        PudosService.default.updateProfile(firstName: user.firsname, lastName: user.lastname, mobile: user.mobile, birthDate: user.birthdate, oldPassword: old, newPassword: new, confirmPassword: confirm, file: user.image, pictureAction: action , invite: user.invite!).then{ (response) -> Void in
                let result = response.result
            
                if result.isSuccessful {
                    
                    self.alertManager?.showAlert(message: response.result, color: AppColor.main.green)
                    
                } else if result.noAuth {
                    SessionManager.sharedInstance.invalidate()
                    RootController.default.initialeViewController()
                    
                } else {
                    guard let message = response.message else {return}
                    self.alertManager?.showAlert(message: message)
            }
                
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
        
        
        
        
        
        
    }
    

}

enum ProfileAction: String {
    case file = "file"
    case `default` = "default"
}
