//
//  SearchOrderViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 25/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown


class SearchOrderViewController: UIViewController {

    
    @IBOutlet weak var orderNumberSwitch: UISwitch!
    @IBOutlet weak var orderNumberTextField: UITextField!
    
    @IBOutlet weak var orderStatusSwitch: UISwitch!
    @IBOutlet weak var orderStatusTextField: UITextField!
    
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var fromDateSwitch: UISwitch!
    @IBOutlet weak var fromDatePicker: UIDatePicker!
    
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var toDatePicker: UIDatePicker!
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBOutlet weak var contentView: UIView!
    
    private let dropDown = DropDown()
    
    
    private let keyboardManager = KeyboardHideManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardManager.targets = [view]
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        self.dismissButton.addGestureRecognizer(tap)
        
        
        self.fromDatePicker.addTarget(self, action: #selector(pickerValueChange), for: .valueChanged)
         self.toDatePicker.addTarget(self, action: #selector(pickerValueChange), for: .valueChanged)
        
        
        orderStatusTextField.delegate = self
        orderStatusTextField.inputView = UIView()
        
        self.dropDown.anchorView = orderStatusTextField
        
        self.dropDown.dataSource = PudosRepository.search
        
        self.dropDown.width = 100
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.orderStatusTextField.text = item
            
        }
    
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true)
    }

   

    @IBAction func didTapSearchButton(_ sender: Any) {
        
        var id = ""
        var status = ""
        var from = ""
        var to = ""
        
        if orderNumberSwitch.isOn {
            id = self.orderNumberTextField.text ?? ""
        }
        if orderStatusSwitch.isOn {
            let item = self.orderStatusTextField.text ?? ""
            status = PudosRepository.status[item] ?? ""
        }
        if fromDateSwitch.isOn {
            from = String(fromDatePicker.date.description.split(separator: " ")[0])
            to = String(toDatePicker.date.description.split(separator: " ")[0])
        }
        
        
        
       let search = Searchable(id, status, from, to)
        
       let vc = LookUpOrdersViewController.instantiate(search: search)
        self.show(vc, sender: nil)
        
        
       
        
    }
    
   
    
    
}
extension SearchOrderViewController {
    @objc func pickerValueChange(_ sender: Any) {
        let picker = sender as! UIDatePicker
        let tag = picker.tag
        
        switch tag {
        case 1:
            let date = String(fromDatePicker.date.description.split(separator: " ")[0])
            self.fromDateLabel.text = customDate(date: date)
            
        case 2:
            let date = String(toDatePicker.date.description.split(separator: " ")[0])
            self.toDateLabel.text = customDate(date: date)
        default:
            fatalError("No Picker was detected take \(tag)  TAG")
        }
    }
}
extension SearchOrderViewController {
    
    func customDate(date description: String) -> String {
        let substr = description.split(separator: "-")
        let year = String(substr[0])
        let mon = Int(substr[1])!
        let month = Helper.instance.month[mon - 1]
        let day = String(substr[2])
        
        return day + " " + month + " " + year
        
        
    }
    
    
}
extension SearchOrderViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        dropDown.show()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        dropDown.hide()
    }
    
}

