//
//  LookUpOrdersViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 05/03/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

typealias Searchable = (orderId:String,status: String,from: String,to: String)

class LookUpOrdersViewController: UIViewController {
    
    

    @IBOutlet weak var tableView: UITableView!
    private var searchable: Searchable = ("","","","")
    private var orders: [Order] = []
    
    
    static func instantiate(search: Searchable) -> LookUpOrdersViewController {
        
        
        let identifier = String(describing: self)
        let vc = UIStoryboard(name: "Pudos", bundle: nil).instantiateViewController(withIdentifier: identifier) as! LookUpOrdersViewController
        vc.searchable = search
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(MyOrdersTableViewCell())
        
        fetshData(id: searchable.orderId, status: searchable.status, start: searchable.from, end: searchable.to, page: 1)
        
    }
    
    @IBAction func didTapDismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension LookUpOrdersViewController: UITableViewDelegate {
    
    
}
extension LookUpOrdersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return orders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as MyOrdersTableViewCell
        //cell.detailDelegate = self
        let order = orders[indexPath.row]
        cell.bind(order: order)
        
        return cell
    }
    
    
    
    
}
extension LookUpOrdersViewController {
    
    fileprivate func fetshData(id: String, status: String, start: String, end: String, page: Int)  {
        
        self.showSpinner()
        
        PudosService.default.findOrder(id: id, status: status, start: start, end: end, page: page).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                
                guard let orders = response.orders else { return }
                self.orders = orders
                self.tableView.reloadData()
                
                
            } else if result.noAuth {
                
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                print("error --")
                
            }
            
            
            }.always {
                self.hideSpinner()
            }.catch(execute: { (error) in
                print(error.localizedDescription)
            })
    }
    
}
