//
//  MyOrdersTableViewCell.swift
//  PUDOS
//
//  Created by Majid Inc on 04/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwipeCellKit


class MyOrdersTableViewCell: SwipeTableViewCell, ReuseableView, NibLoadableView {
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var creationLabel: UILabel!
    
    @IBOutlet weak var statusImageView: CercleImage!
    
    weak var detailDelegate: DetailOrderDelegate!
    
    var orderId: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(order: Order) {
        
        self.orderId = order.id
        self.statusLabel.text = order.statusStr
        self.creationLabel.text = order.created
        if order.isOpen() {
            self.statusImageView.backgroundColor = AppColor.main.green
        } else if order.isCancelled() {
            self.statusImageView.backgroundColor = AppColor.main.red
        } else {
            self.statusImageView.backgroundColor = AppColor.main.blue
        }
        
    }
    
    
    @IBAction func didTapDetailButton(_ sender: Any) {
        if let id = orderId {
            self.detailDelegate.detail(id)
        }
        
    }
    
    
    private enum Status: String {
        case open = "Open"
        case close = "Close"
    }
    
}
