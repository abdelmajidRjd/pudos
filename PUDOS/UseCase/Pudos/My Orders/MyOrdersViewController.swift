//
//  MyOrdersViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

private let searchHeight: CGFloat = 100.0

class MyOrdersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var orders: [Order]?
    private var selectedOrder =  [Order]()
    private var orderId: String?
    
    @IBOutlet var heightConstraintLayout: NSLayoutConstraint!
    private var isSearching: Bool = false
    
    private var isOpen: Bool = true
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(MyOrdersTableViewCell())
     
        fetshData(id: "", status: "", start: "", end: "", page: 1)
    
    }
    
    
    @IBAction func didStatusValueChange(_ sender: UISegmentedControl) {
        
                self.isOpen = !self.isOpen
                fillTable()
        
        
    }
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        let height: CGFloat = 40.0
        
        heightConstraintLayout.constant = isSearching ? searchHeight : height
        isSearching = !isSearching
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let identifier = segue.identifier!
        if identifier == "showDetail" {
            let dest = segue.destination as! OrderDetailPolyLineViewController
            dest.orderId = orderId ?? ""
            
        }
        

    }
    
}

extension MyOrdersViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
    }
    
}

extension MyOrdersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedOrder.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as MyOrdersTableViewCell
        cell.detailDelegate = self
        let order = selectedOrder[indexPath.row]
        cell.bind(order: order)
        
        return cell
    }

}
extension MyOrdersViewController {
    
    fileprivate func fetshData(id: String, status: String, start: String, end: String, page: Int)  {
        
        self.showSpinner()
        
        PudosService.default.findOrder(id: id, status: status, start: start, end: end, page: page).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                
                guard let orders = response.orders else { return }
                self.orders = orders
                self.fillTable()
               
                
            } else if result.noAuth {
                
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                print("error --")
                
            }
            
            
            }.always {
                self.hideSpinner()
            }.catch(execute: { (error) in
                print(error.localizedDescription)
            })
    }
    
    private func fillTable(){
        
        self.selectedOrder.removeAll()
        if isOpen {
            
            self.orders?.forEach({ (order) in
                if order.isOpen(){
                    self.selectedOrder.append(order)
            }
            })
        } else {
            
            self.orders?.forEach({ (order) in
                if order.isCancelled(){
                    self.selectedOrder.append(order)
                }
            })
            
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.view.layoutIfNeeded()
        }
    
    
    }
    
}
extension MyOrdersViewController: SearchOrderDelegate {
   
    func search(order id: String, status: String, from: String,to: String) {
        
        fetshData(id: id, status: status, start: from, end: to, page: 1)
        
    }
}
protocol SearchOrderDelegate : class{
    func search(order id: String, status: String,from:String,to: String)
}

protocol DetailOrderDelegate: class{
    func detail(_ orderId: String)
}
extension MyOrdersViewController: DetailOrderDelegate {
    func detail(_ orderId: String) {
        self.orderId = orderId
        self.performSegue(withIdentifier: "showDetail", sender: nil)
    }
}
