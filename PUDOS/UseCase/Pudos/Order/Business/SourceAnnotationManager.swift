//
//  SourceAnnotationManager.swift
//  PUDOS
//
//  Created by Majid Inc on 09/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import UIKit


protocol SourceAnnotationProtocol {
    func add(annotation: PudosAnnotation, order: ORDER)
    func isEligible() -> Bool
}

class SourceAnnotationManager:  SourceAnnotationProtocol{
    
    private var orders : [PudosAnnotation:ORDER]
    private var destination : PudosAnnotation
    
     init(destination: PudosAnnotation) {
        self.destination = destination
        orders = [PudosAnnotation:ORDER]()
    }
    
    private func isAlreadyAdded(testAnnotation: PudosAnnotation) -> Bool {
        
        var isExist: Bool = false
        orders.forEach { (annotation,_) in
            if annotation == testAnnotation {
                isExist = true
            }
        }
        
        return isExist
    }
    
     private func update(annotation: PudosAnnotation, order: ORDER) {
        
        orders.forEach { (realAnnotation, oldOrder) in
            if realAnnotation == annotation {
                orders[annotation] = order
            }
        }
    }
    
     func add(annotation: PudosAnnotation, order: ORDER){
        
        if isAlreadyAdded(testAnnotation: annotation) {
            
            update(annotation: annotation, order: order)
        } else {
            
            orders[annotation] = order
        }
    }
    func isEligible() -> Bool {
        
        if orders.count > 0 && destination.address() != nil{
                return true
        }
        return false 
        
    }
   
    func optimize() -> Bool? {
        return orders.first?.value.optimize
    }
    
    func imageItems() -> [Data] {
        var data =  [Data]()
        self.orders.forEach { (_, order) in
            data.append(contentsOf: order.itemImages)
        }
        
        return data
    }
    
    func sourceWayPoints() -> [WayPoint] {
        
        var wayPoints = [WayPoint]()
        orders.forEach { (annotation, order) in
            guard let addr = annotation.address() else {  return}
            let name = addr.name
            let state = addr.state
            let latitude = annotation.coordinate.latitude
            let longitude = annotation.coordinate.longitude
            
            let location = Location(lat: Double(latitude), lng: Double(longitude))
            
            let wayPoint = WayPoint(name: name, address: addr, state: state.state, zipCode: state.zipCode, description: order.desc, type: order.type, data: order.data, regul: order.regul, location: location)
            
            
            wayPoints.append(wayPoint)
            
            
            
        }
        return wayPoints
        
    }
 
 
    
    
}
