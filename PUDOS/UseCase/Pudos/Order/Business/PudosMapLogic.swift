//
//  PudosMapLogic.swift
//  PUDOS
//
//  Created by Majid Inc on 08/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import MapKit

protocol PudosLogic {
    func add(annotation: PudosAnnotation )
}

class PudosMapBusinessLogic : PudosLogic {
   
    
    
    private var sourceAnnotations : [PudosAnnotation]
    private let destination: PudosAnnotation
    private var mapView: MKMapView
    
    init(source:[PudosAnnotation] ,destination: PudosAnnotation, mapView: MKMapView) {
        self.destination = destination
        self.mapView = mapView
        self.sourceAnnotations = source
        self.sourceAnnotations.append(destination)
    }
    
     func add(annotation: PudosAnnotation ) {
        self.sourceAnnotations.append(annotation)
    }
    
     func remove(annotation: PudosAnnotation) {
        self.sourceAnnotations.removeObject(obj: annotation)
    }
    
    private func inPinAnnotations() -> [PudosAnnotation] {
        return self.sourceAnnotations.filter({ (annotation) -> Bool in
            return annotation.isSearchable()
        })
    }
    
    func changeCoordinate(coordinate: CLLocationCoordinate2D){
        
        let inpinAnnotations = inPinAnnotations()
        
        mapView.removeAnnotations(inpinAnnotations)
        for annotation in inpinAnnotations {
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
        }
        
    }
    

}

extension Array {
    mutating func removeObject<T>(obj: T) where T : Equatable {
        self = self.filter({$0 as? T != obj})
    }
    
}
