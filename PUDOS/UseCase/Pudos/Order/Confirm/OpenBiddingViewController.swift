//
//  ConfirmOrderViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 05/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class OpenBiddingViewController: UIViewController {
    
    
    @IBOutlet weak var totalDistanceLabel: UILabel!
    @IBOutlet weak var approxDurationLabel: UILabel!
    @IBOutlet weak var cashLabem: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    
    
    var orderInfo: OrderMapResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let orderInfo = orderInfo else {

            show(error: "No Order was sent")
            return
        }
        
        self.totalDistanceLabel.text = String(orderInfo.distance)
        self.approxDurationLabel.text = String(orderInfo.duration)
        self.cashLabem.text = orderInfo.reimb
        
    }
    
    
    @IBAction func didTapOpenBiddingButton(_ sender: Any) {
        
        guard let order = orderInfo else {
            show(error: "Order Not Complete")
            return
            
        }
        showSpinner()
        PudosService.default.confirmOrder(method: "nin", orderId: order.id).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                
                RootController.default.home()
                
                
            } else if result.noAuth {
                
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                
                self.show(error: response.message ?? "Error")
            }
            
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
        
    }
    
}
