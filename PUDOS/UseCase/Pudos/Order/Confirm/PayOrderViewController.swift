//
//  PayOrderViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 07/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PayOrderViewController: UIViewController {

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var reimboursementMethodLabel: UILabel!
    
    @IBOutlet weak var useCreditLabel: UILabel!
    @IBOutlet weak var userInAppLabel: UILabel!
    
    var orderInfo: OrderMapResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let orderInfo = orderInfo else {return}
        
        self.distanceLabel.text = String(orderInfo.distance)
        self.durationLabel.text = String(orderInfo.duration)
        self.reimboursementMethodLabel.text = orderInfo.reimb
        
        self.useCreditLabel.text = String(orderInfo.price)
        self.userInAppLabel.text = String(orderInfo.price)
    }

   
    
    @IBAction func didTapUseCreditCardButton(_ sender: Any) {
        
        guard let order = orderInfo else {
            show(error: "Order Not Complete")
            return
            
        }
        showSpinner()
        PudosService.default.confirmOrder(method: "card",orderId: order.id).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                RootController.default.home()
                
            } else if result.noAuth {
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
            } else {
                self.show(error: response.message ?? "Error")
            }
            
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
    }
    
    
    
    @IBAction func didTapNotEnoughButton(_ sender: Any) {
        guard let order = orderInfo else {
            show(error: "Order Not Complete")
            return
            
        }
        PudosService.default.confirmOrder(method: "credit",orderId: order.id).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                RootController.default.home()
                
            } else if result.noAuth {
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                self.show(error: response.message ?? "Error")
                
            }
            
            }.always {
                self.hideSpinner()
            }.catch { (error) in
                self.show(error: error)
        }
    }
    
}
