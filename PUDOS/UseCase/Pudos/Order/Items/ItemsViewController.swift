//
//  ItemsViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 05/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

typealias ORDER = (desc: String,regul: Bool, data: String, type: Int,itemImages: [Data],optimize: Bool)

protocol ItemSaveOrderProtocol: class {
    func save(order: ORDER)
}


class ItemsViewController: UIViewController {
    
    // MARK: - SEGUE
    private let segueIdentifier = "showDropOffSegue"
    
    @IBOutlet weak var sourceOrderLabel: UILabel!
    @IBOutlet weak var pickUpTextField: UITextField!
    @IBOutlet weak var orderIdTextField: UITextField!
    
    @IBOutlet weak var isPrepaidLabel: UILabel!
    
    @IBOutlet weak var alcoholSwitch: UISwitch!
    @IBOutlet weak var prepaidSwitch: UISwitch!
    @IBOutlet weak var optimizeWayPoints: UISwitch!
    
    // MARK: - delegate
    weak var  delegate : ItemSaveOrderProtocol?
    
    // MARK: - Manager
    private let keyboardManager = KeyboardHideManager()
    
    
    // MARK: - Collection Views
    @IBOutlet weak var ImageCollectionView: UICollectionView!
    private var images: [UIImage]?
    private var data = [Data]()
    
    private var  imagePickerController = UIImagePickerController()
    
    var sourceAnnotation: PudosAnnotation?
    
    // MARK: - Properties : Data to send
    
    
    
    var sourceLocation: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        keyboardManager.targets = [view]
        
        self.imagePickerController.delegate = self
        self.ImageCollectionView.delegate = self
        self.ImageCollectionView.dataSource = self
        self.ImageCollectionView.register(PictureCollectionViewCell())
        
         self.prepaidSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        
        
//        guard let (source, dest) = getAddress() else {
//            return
//        }
//        
//        
//        self.address = (source,dest)
//        
        self.sourceOrderLabel.text = sourceAnnotation?.address() ?? "Unknown"
        
        // Do any additional setup after loading the view.
    }
    
    
        
    @objc func switchChanged(mySwitch: UISwitch) {
        
        UIView.transition(with: isPrepaidLabel,
                          duration: 0.4,
                          options: .curveEaseIn,
                          animations: {
            if mySwitch.isOn {
                self.isPrepaidLabel.text = "$"
                self.orderIdTextField.placeholder = "Cash Amount"
            } else {
                self.isPrepaidLabel.text = "ID"
                self.orderIdTextField.placeholder = "Order ID"
            }
        })
        self.view.layoutIfNeeded()
    
    }
    
    

    // MARK: - Actions
    @IBAction func didTapAddPictureButton(_ sender: UIButton) {
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true)
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        // Logic
        
        guard let orderDesc = pickUpTextField.text else { return }
        let type = prepaidSwitch.isOn ? 1 : 2
        let alcohol = alcoholSwitch.isOn
        let optimize = optimizeWayPoints.isOn
        guard let orderId = orderIdTextField.text else { return }
        
        if orderDesc.trimmingCharacters(in: .whitespaces).isEmpty {
            let error = NSError(domain: "OrderDescDomain", code: 1)
            self.show(error: error)
            return
        }
        guard let _ = Float(orderId) else {
            let error = NSError(domain: "CastOrderIdDomain", code: 1)
            self.show(error: error)
            return
        }
        

        let order = (orderDesc, alcohol, orderId, type, data, optimize)
        
        
        self.delegate?.save(order: order)
        
        dismiss(animated: true)
        
    }
    
    
    @IBAction func didTapExitButton(_ sender: Any) {
        dismiss(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let identifier = segue.identifier!
        switch identifier {
        case "showHelp":
            let destination = segue.destination as! Help2ViewController
        destination.popoverPresentationController?.delegate = self
            
        default:
            fatalError()
        }
    }
    
    // MARK: - Help Button
    
    
    @IBAction func didTapPrepaidHelpButton(_ sender: Any) {
        
        let button = sender as! UIButton
        let helpVC = StoryBoardManager<Help2ViewController>().instantiate(from: .Pudos)
        helpVC.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = helpVC.popoverPresentationController
        popover?.delegate = self
        popover?.sourceView = button
        self.present(helpVC, animated: true, completion: nil)
        
    }
    
    @IBAction func didTapTobaccoHelpButton(_ sender: Any) {
        let button = sender as! UIButton
        let vc = HelpViewController.instantiate(description: "Check this box if you are picking up items that contain regulated substances such as Tabacco or Alcohol. When this case is checked, you will be asked to produce a legal document to the butler so that they can verify your age.")
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.preferredContentSize = CGSize(width:200, height: 300)
        let popover = vc.popoverPresentationController
        popover?.delegate = self
        
        popover?.sourceView = button
        self.present(vc, animated: true, completion: nil)
        
    }
    
}

extension ItemsViewController: UICollectionViewDelegate {
}

extension ItemsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(indexPath: indexPath) as PictureCollectionViewCell
        
        if let images = images {
            let image = images[indexPath.row]
            cell.delegate = self
            cell.bind(image: image)
        }
        
        return cell
    }
   
}
// MARK - Photo
extension ItemsViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        if let data = UIImagePNGRepresentation(image) {
            self.data.append(data)
        }
        if let _ = images {
            self.images?.append(image)
        } else {
            self.images = [image]
        }
        self.ImageCollectionView.reloadData()
        
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
// MARK: - Deletion Event
extension ItemsViewController: PictureCollectionViewDelegate {
    
    func didTapTrash(sender: PictureCollectionViewCell) {
        
        guard let indexPath = self.ImageCollectionView.indexPath(for: sender) else {return}
       
        self.images?.remove(at: indexPath.row)
        self.ImageCollectionView.deleteItems(at: [indexPath])
        self.ImageCollectionView.reloadData()
        
    }
}


extension ItemsViewController : UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}


