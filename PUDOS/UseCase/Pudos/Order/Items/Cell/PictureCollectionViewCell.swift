//
//  PictureCollectionViewCell.swift
//  PUDOS
//
//  Created by Majid Inc on 06/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PictureCollectionViewCell: UICollectionViewCell, NibLoadableView, ReuseableView {

    weak var delegate: PictureCollectionViewDelegate?

    
    @IBOutlet weak var pictureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        delegate?.didTapTrash(sender: self)
    }
    
    func bind(image: UIImage) {
        self.pictureImageView.image = image
    }
    
}
protocol PictureCollectionViewDelegate: class {
    func didTapTrash(sender cell: PictureCollectionViewCell)
}
