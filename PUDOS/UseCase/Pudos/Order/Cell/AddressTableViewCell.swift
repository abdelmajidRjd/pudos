//
//  AddressTableViewCell.swift
//  PUDOS
//
//  Created by Majid Inc on 09/03/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell, NibLoadableView, ReuseableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    public var coordinate: Coordinate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(_ coordinate: Coordinate) {
        
        self.titleLabel.text = coordinate.fullAddress.split(separator: " ").first!.description
        self.descriptionLabel.text = coordinate.fullAddress
        self.coordinate = coordinate
    }
    
}
