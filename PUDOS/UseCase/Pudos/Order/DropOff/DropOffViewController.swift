//
//  DropOffViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 05/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import ObjectMapper


class DropOffViewController: UIViewController {
    // MARK: - Business Logic
    
    private let DESTINATION_TYPE = 4
    private let NEED_IT_NOW = 1
    
    
    // MARK: - Segue Identifier
    private let openBiddingSegue = "openBiddingSegue"
    private let payCardSegue = "payCardSegue"

    @IBOutlet weak var fullAddressTextField: UITextField!
    
    @IBOutlet weak var reimboursementMethodTextField: UITextField!
    
    @IBOutlet weak var deliverySpeedTextField: UITextField!
    
    @IBOutlet weak var promoCodeTextField: UITextField!
    
    @IBOutlet weak var additionalTextView: UITextView!
    
    private let keyboardManager = KeyboardHideManager()
    
    private let dropDoreimboursementMethod = DropDown()
    
    private let dropDownRein = DropDown()
    
    private let dropDownDelivery = DropDown()
    
    private let dropDownWidth: CGFloat = 100
    
    // MARK : - Properties : Received
    var order: ORDER?
    private var wayPoints: (source: [WayPoint],destination: WayPoint)?
    
    // MARK : - Properties : to Sent
    private var userOrder: OrderMapResponse?
    
    private var destination: PudosAnnotation?
    private var sourceManager : SourceAnnotationManager?
    
    
    @IBAction func didTapDismissButton(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        keyboardManager.targets = [view]
        
        
        guard let (sourceManager,dest) = getAddress() else {
            return
        }
        
        self.sourceManager = sourceManager
        self.destination = dest
        
        self.fullAddressTextField.text = dest.subtitle ?? ""
        
      
        
        if (fullAddressTextField.text?.isEmpty)! {
                let error = NSError(domain: "FullAddressDomain", code: 1)
                self.show(error: error)
                return
        }
       
        
        guard let destiAddress = dest.address() else {return}
        let destinationName = destiAddress.name
        let destinationState = destiAddress.state
        
        let destinationlat = Double(dest.coordinate.latitude)
        let destinationlng = Double(dest.coordinate.longitude)
        let destLocation = Location(lat: destinationlat, lng: destinationlng)
        
        let destinationWayPoint = WayPoint(name: destinationName, address: destiAddress, state: destinationState.state, zipCode: destinationState.zipCode, description: nil, type: DESTINATION_TYPE, data: nil, regul: nil, location: destLocation)
        
        
        self.wayPoints = (sourceManager.sourceWayPoints(), destinationWayPoint)
        
        reimboursementMethodTextField.text = PudosRepository.reimb.first
        deliverySpeedTextField.text = PudosRepository.delivery.first
        
        
        self.dropDownRein.anchorView = reimboursementMethodTextField
        self.dropDownDelivery.anchorView = deliverySpeedTextField
  
        self.dropDownDelivery.dataSource = PudosRepository.delivery
        self.dropDownRein.dataSource = PudosRepository.reimb
        
        self.dropDownRein.width = dropDownWidth
        self.dropDownDelivery.width = dropDownWidth * 2
        
        
        self.dropDownRein.selectionAction = { [unowned self] (index: Int, item: String) in
            self.reimboursementMethodTextField.text = item
        }
        
        self.dropDownDelivery.selectionAction = { [unowned self] (index: Int, item: String) in
            self.deliverySpeedTextField.text = item
        }
        
        if let address = getAddress() {
            fullAddressTextField.text = address.dest.address() ?? "Issue"
            self.hideSpinner()
        }
    }
    
    
    @IBAction func didTapReimButton(_ sender: Any) {
        self.dropDownRein.show()
    }
    
    @IBAction func didTapDeliveryButton(_ sender: Any) {
        self.dropDownDelivery.show()
    }
    
    
    
    
    
    
    @IBAction func didTapNextButton(_ sender: Any) {
        
        self.showSpinner()
        
        guard let additional = additionalTextView.text else {return}
        
        guard let promoCode = promoCodeTextField.text else {return}
        
        guard let reim = reimboursementMethodTextField.text else {return}
        guard let deliver = deliverySpeedTextField.text else {return}

        if !promoCode.trimmingCharacters(in: .whitespaces).isEmpty
            || Float(promoCode) != nil{
            let error = NSError(domain: "PromoCodeDomain", code: 1)
            self.show(error: error)
            return
        }
        
        
        
        
        guard let sourceWayPoint = self.wayPoints?.source.first else { return }
        guard let destinationWayPoint = self.wayPoints?.destination else { return }
        
        guard let reimboursement = PudosRepository.reimbouresement[reim] else {return}
        guard let delivery = PudosRepository.deliverySpeed[deliver] else {return}
        
          // data regul
        
        let wayPoints = WayPoints(waypoints: [sourceWayPoint, destinationWayPoint])
        
        guard let json = Mapper().toJSONString(wayPoints) else {return}
        
        guard let sourceManager = sourceManager else {
            let error = NSError(domain: "OrderDescDomain", code: 1)
            self.show(error: error)
            return
        }
        
      
        PudosService.default.createOrder(wayPoints: json, instructions: additional, reimb: reimboursement, speed: delivery, itemImages: sourceManager.imageItems(), promo: promoCode, optimize: sourceManager.optimize()!).then { (response) -> Void in
            
            let result = response.result
            if result.isSuccessful {
                
                if let userOrder = response.order {
                    
                    self.userOrder = userOrder
                
                    if delivery == self.NEED_IT_NOW {
                    self.performSegue(withIdentifier: self.openBiddingSegue, sender: nil)
                } else {
                    self.performSegue(withIdentifier: self.payCardSegue, sender: nil)
                }
                    
                }
                
            } else if result.noAuth {
    
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                
               self.show(error: response.message ?? "Error")
                
            }
            
            }.always {
                self.hideSpinner()
            }
 
        
    }
    // MARK : - Help View Con
    
    @IBAction func didTapReimHelpButton(_ sender: UIButton) {
        showHelp(source: sender, description: "This is the method you would like to use to compensate the butler for the money they laid out to buy items on your behalf, as well as road tolls if any.")
    }
    
    
    
    @IBAction func didTapNeeditNowButton(_ sender: UIButton) {
        
        showHelp(source: sender, description: "Choose how fast you want your items to be delivered. This also affect how long your order will be available for butlers to pickup.")
    }
    
    private func showHelp(source: UIButton, description: String) {
        let vc = HelpViewController.instantiate(description: description)
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.preferredContentSize = CGSize(width: 300, height: 400)
        let popover = vc.popoverPresentationController
        popover?.delegate = self
        
        popover?.sourceView = source
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}
extension DropOffViewController {
    private func toJSON(wayPoint: WayPoint) -> [String:Any] {
        
        
        let map = Map.init(mappingType: .toJSON, JSON: ["data" : ["name" : wayPoint.name,"addr":wayPoint.address,"state":wayPoint.state,"zipcode":wayPoint.zipCode,"desc": wayPoint.description ?? "","regul": wayPoint.regul ?? "" ,"type":wayPoint.type,"data":wayPoint.data ?? "","location":["lat":wayPoint.location.lat ,"lng":wayPoint.location.lng]]])
        
        return map.JSON
        
    }
}
extension DropOffViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {return}
        switch identifier {
            
            case payCardSegue:
                 let destination = segue.destination as! PayOrderViewController
                 destination.orderInfo = self.userOrder
            
            case openBiddingSegue:
                let destination = segue.destination as! OpenBiddingViewController
                destination.orderInfo = self.userOrder
               
        default:
            let error = NSError(domain: "", code: -1)
            show(error: error)
        }
        
    }
    
}

extension DropOffViewController {
    
    func create(){
        
        PudosService.default.createOrder(wayPoints: "jj", instructions: "additional", reimb: 1,  speed: 1, itemImages: [Data()], promo: nil, optimize: true).then { (response) -> Void in
            
            let result = response.result
            
            if result.isSuccessful {
                    
            } else if result.noAuth {
                
                    SessionManager.sharedInstance.invalidate()
                    RootController.default.initialeViewController()
                
            } else {
                
                self.show(error: response.message ?? "Error")
                
            }
            
            }.always {
                self.hideSpinner()
        }
        
        
    }
    
}
extension DropOffViewController {
    
    func getAddress()->(source: SourceAnnotationManager, dest: PudosAnnotation)?{
      let parent = self.navigationController as! OrderNavViewController
      guard let source = parent.sourceManager,
      let dest = parent.dest else {
      return nil
     }
      return (source, dest)
     
     }
}
extension DropOffViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
}
