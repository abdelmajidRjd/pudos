//
//  LinkedCardTableViewCell.swift
//  PUDOS
//
//  Created by Majid Inc on 02/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwipeCellKit

class LinkedCardTableViewCell: SwipeTableViewCell, ReuseableView, NibLoadableView {
    
    @IBOutlet weak var expirationDateLabel: UILabel!
    
    @IBOutlet weak var last4Label: UILabel!
    
    @IBOutlet weak var brandImageView: UIImageView!
    
    var card: Card?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(card: Card) {
        
        self.card = card
        
        self.expirationDateLabel.text = card.exp
        self.last4Label.text = card.last4
        
        brand(brand: card.brand)
        
        if card.current {
            self.backgroundColor = AppColor.main.pudosSecondaryColor
        }
    
        
    }
    private func brand(brand: String) {
        
        if brand.uppercased() == "visa" {
            self.brandImageView.image = #imageLiteral(resourceName: "icons8-visa-100")
        } else {
            self.brandImageView.image = #imageLiteral(resourceName: "icons8-mastercard-credit-card-100")
        }
        
    }
    
}




