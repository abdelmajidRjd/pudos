//
//  FundSourceViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 31/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwipeCellKit

class FundSourceViewController: UIViewController {
    

    @IBOutlet weak var linkedCardTableView: UITableView!
    
    private var cards: [Card]?
    
    //private var linkedCards: [
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showSpinner()
        linkedCardTableView.delegate = self
        linkedCardTableView.dataSource = self
        linkedCardTableView.register(LinkedCardTableViewCell())
        
        
        PudosService.default
            .getCreditCards()
            .then { (pudosCardResponse) -> Void in
                
                if pudosCardResponse.result.isSuccessful {
                    
                    guard let cards = pudosCardResponse.cards else {
                        return
                    }
                    
                    self.cards = cards
                    self.linkedCardTableView.reloadData()
                    self.hideSpinner()
                    
                } else {
                    
                }
            
        }
  
       
    }
    
    @IBAction func didTapAddCardsButton(_ sender: Any) {
        
        
    }
    
}
extension FundSourceViewController: UITableViewDelegate {
    
}

extension FundSourceViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as LinkedCardTableViewCell
        
        guard let cards = cards else {
          return LinkedCardTableViewCell()
        }
        let card = cards[indexPath.row]
        
        cell.bind(card: card)
        cell.delegate = self 
        
        return cell
    }
    
    
}

extension FundSourceViewController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let cell = tableView.cellForRow(at: indexPath) as! LinkedCardTableViewCell
        
    
        guard  let card = cell.card else {return nil}
        
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { action, indexPath in
            
            PudosService.default.removeCreditCard(stripToken: card.id).then(execute: { (response) -> Void in
                
                        if response.result.isSuccessful {
                            self.linkedCardTableView.reloadData()
                        }else {
                            print(response.message!)
                            }
        
             }).catch(execute: { (error) in
                self.show(error: error)
             })
            
        }
        
        // customize the action appearance
        deleteAction.image = #imageLiteral(resourceName: "ic_highlight_off_white_48pt")
        deleteAction.backgroundColor = AppColor.main.blueLight
        
        
        return [deleteAction]
    }
}
