//
//  AddCardViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 02/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown
import Stripe


class AddCardViewController: UIViewController {
    
    @IBOutlet weak var cardNumberTextField: PudosTextField!
    
    @IBOutlet weak var expYearTextField: UITextField!
    
    @IBOutlet weak var expMonthTextField: UITextField!
    
    @IBOutlet weak var cvcTextField: PudosTextField!
    
    @IBOutlet weak var contentView: UIStackView!
    
    private let monthDropDown = DropDown()
    private let yearDropDown = DropDown()
    
    private var alertManager :AlertViewHandler?
    
    let keyboardManager = KeyboardHideManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertManager = AlertViewHandler(contentStackView: contentView)
        
        monthDropDown.anchorView = self.expMonthTextField
        yearDropDown.anchorView = expYearTextField
        
        
        
        
        keyboardManager.targets = [view]


        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapAddCardButton(_ sender: Any) {
        
        
        showSpinner()
        guard let number = cardNumberTextField.text,
             let expMonth = Int(expMonthTextField.text!),
             let expYear = Int(expYearTextField.text!),
             let cvc = cvcTextField.text else {
                
                // show an error
                let error = NSError(domain: "AddCardTextValidationError", code: 1)
                self.show(error: error, close: true)
                
                return
        }
        
        let cardPayment = STPCardParams()
            cardPayment.number = number.eliminateSpace()
            cardPayment.cvc = cvc.eliminateSpace()
            cardPayment.expMonth = UInt(expMonth)
            cardPayment.expYear = UInt(expYear)
        
  
        STPAPIClient.shared().createToken(withCard: cardPayment) { (token, error) in
            
            guard let stripeToken = token, error == nil else {
                
                self.alertManager?.showAlert(message: (error?.localizedDescription)!)
                self.hideSpinner()
                return
            }
            
            PudosService.default.addCreditCard(stripToken: stripeToken.tokenId).then(execute: { (response) -> Void in
                
                if response.result.isSuccessful {
                    
                    self.alertManager?.showAlert(message: "your card was added successfully", color: AppColor.main.green)
                    
                    self.navigationController?.popViewController(animated: true)
                    
                } else if response.result.noAuth {
                    
                    SessionManager.sharedInstance.invalidate()
                    RootController.default.initialeViewController()
                    
                } else {
                    self.alertManager?.showAlert(message: response.message!)
                }
                
            }).always {
                self.hideSpinner()
            }
            
        }
        
        
        
    }
    

    @IBAction func didTapDismissButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
extension String {
    
    func eliminateSpace() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
