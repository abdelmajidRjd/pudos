//
//  NotificationTableViewCell.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwipeCellKit

class NotificationTableViewCell: SwipeTableViewCell, ReuseableView, NibLoadableView {
    
    @IBOutlet weak var notificationImageView: UIImageView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    var pudosId: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(notification: NotificationData) {
        self.pudosId = notification.id
        if let pudosId = PudosHtmlParser.pudosId(message: notification.message) {
            
            let message =  PudosHtmlParser.message(message: notification.message) + String(pudosId)
            self.messageLabel.attributedText = NSAttributedString(string: message)
            
        }
        
        
    }
    
}
