//
//  NotificationViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 31/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import SwipeCellKit

class NotificationViewController: UIViewController {
    
    
    
    @IBOutlet weak var notificationTableView: UITableView!
    
    @IBOutlet weak var notifNumberLabel: UILabel!
    
    private var notifications: [NotificationData]?
    private var refreshControl = UIRefreshControl()
    
    private var selectedNotification: NotificationData?

   
    
    fileprivate func fetshNotification()  {
        
        showSpinner()
         PudosService
            .default
            .getAllNotification().then { (pudosNotification) -> Void in
                
                
                if pudosNotification.result.isSuccessful {
                    let count = pudosNotification.count ?? 0
                    self.tabBarController?.tabBar.items![2].badgeValue = "\(count)"
                    self.notifNumberLabel.text = "You have \(count)  Notifications"
                    
                    self.notifications = pudosNotification.data
                    self.notificationTableView.reloadData()
                } else {
                    print(pudosNotification.result)
                }
                
            }.always {
                
                self.hideSpinner()
                
            }.catch { (error) in
                
                self.show(error: error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
        
        notificationTableView.register(NotificationTableViewCell())
        
        setupRefreshControll()

        fetshNotification()
        
    }
    
    @objc private func refresh(refresh: UIRefreshControl) {
        
        fetshNotification()
        refresh.endRefreshing()
        
    }
    
    fileprivate func setupRefreshControll() {
        
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh) , for: .valueChanged)
        
        notificationTableView.addSubview(refreshControl)
    }
    
    
    @IBAction func didTapRefreshButton(_ sender: Any) {
    }
    
    @IBAction func didTapDismissAll(_ sender: Any) {
        
        PudosService.default.dismissAll().then { (response) ->  Void  in
            
            if response.result.isSuccessful {
                
                self.fetshNotification()
                
            } else if response.result.noAuth {
                
                SessionManager.sharedInstance.invalidate()
                RootController.default.initialeViewController()
                
            } else {
                self.show(error: response.message ?? "Error")
            }
            
            }.catch { (error) in
                self.show(error: error)
        }
    }
}

extension NotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        
        self.selectedNotification = notifications?[row]
        
        performSegue(withIdentifier: "showDetail", sender: nil)
        
        }
    
    
}
extension NotificationViewController: UITableViewDataSource {
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notifications?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as NotificationTableViewCell
        
        let notification = notifications![indexPath.row]
        
        cell.bind(notification:  notification)
    
        cell.delegate = self
        
        return cell
    }
}

extension NotificationViewController : SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let cell = tableView.cellForRow(at: indexPath) as! NotificationTableViewCell
        
        
        
        guard orientation == .right else { return nil }
        
        let dismissAction = SwipeAction(style: .default, title: "Dismiss") { action, indexPath in
            
            guard let notificationId = cell.pudosId else { return }
            
            PudosService.default.dismissOne(id: notificationId).then(execute: { (response) -> Void in
                let result = response.result
                if result.isSuccessful {
                    
                    self.fetshNotification()
                } else if result.noAuth {
                    SessionManager.sharedInstance.invalidate()
                    RootController.default.initialeViewController()
                } else {
                    // show alert .. to do
                }
                
            })
           
        }
        
        
        
        
        // customize the action appearance
        dismissAction.image = #imageLiteral(resourceName: "ic_close_48pt")
        dismissAction.backgroundColor = AppColor.main.blue
        dismissAction.highlightedBackgroundColor = AppColor.main.pudosMainColor
        
        return [dismissAction]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {return}
        switch identifier {
        case "showDetail":
            let destination = segue.destination as! OrderDetailPolyLineViewController
            guard let selected = self.selectedNotification, let orderId = PudosHtmlParser.pudosId(message: selected.message) else { return }
            
            
            destination.orderId = orderId
        default:
            print("")
        }
        
    }

    
}


