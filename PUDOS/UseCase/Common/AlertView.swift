//
//  AlertView.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class AlertView: UIView {

    @IBOutlet weak var descriptionImageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    class func instantiate() -> AlertView {
        return Bundle.main.loadNibNamed("AlertView", owner: self, options: nil)?.first as! AlertView
    }
    
    override func layoutSubviews() {
        
        let layer = self.layer
        
        layer.cornerRadius = 10
        
        super.layoutSubviews()
    }
    

}
