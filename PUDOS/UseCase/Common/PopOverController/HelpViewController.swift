//
//  HelpViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 04/03/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    
    private var helpDescription: String?

    static func instantiate(description: String) -> HelpViewController {
        
        let identifier = String(describing: self)
        let vc = UIStoryboard(name: "Pudos", bundle: nil).instantiateViewController(withIdentifier: identifier) as! HelpViewController
        vc.helpDescription = description
        return vc
    }
    
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.descriptionLabel.adjustsFontSizeToFitWidth = true
        self.descriptionLabel.text = helpDescription ?? "Error"
        
    }

}
