//
//  ResetPasswordViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: PudosTextField!
    
    
    @IBOutlet weak var dayTextField: UITextField!
    
    @IBOutlet weak var monthTextField: UITextField!
    
    @IBOutlet weak var yearTextField: UITextField!
    
    @IBOutlet weak var contentView: UIStackView!
    
    private var alertManager: AlertViewHandler?
    
    private let manager = KeyboardHideManager()
    
    private let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.targets = [view]
        alertManager = AlertViewHandler(contentStackView: contentView)
        monthTextField.delegate = self
        
        dropDown.anchorView = monthTextField
        dropDown.dataSource = Helper.instance.month
        dropDown.selectionAction = {  (index: Int, item: String) in
            self.monthTextField.text = String(index + 1)
            self.dropDown.hide()
            
        }

    }
    
    
    

    @IBAction func didTapResetPasswordButton(_ sender: Any) {
        
        showSpinner()
        
        guard let email = emailTextField.text,
             let day = dayTextField.text,
             let month = monthTextField.text,
             let year = yearTextField.text
        else { return }
        
        let bithdate = day+"-"+month+"-"+year
        
        PudosService
            .default
            .resetPassword(email: email, birthDate: bithdate).then
                { (response) -> Void in
                // handle success reset
                    if response.result.isSuccessful {
                        let message = "Password recovery email was sent to the user's email address."
                        self.alertManager?.showAlert(message: message,color: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
                    } else {
                        self.alertManager?.showAlert(message: response.message ?? "Error")
                    }
                    
                
                }.catch { (error) in
                
                
                
                }.always {
                
                self.hideSpinner()
            }
           
        
    }
    
}
extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        dropDown.show()
        
    }
    
    
}
