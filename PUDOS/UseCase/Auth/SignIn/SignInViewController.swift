//
//  SignInViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignInViewController: UIViewController {
    
    
    @IBAction func unwindToSignIn(_ segue: UIStoryboardSegue){
        //
    }

    @IBOutlet weak var contentView: UIStackView!
    
    @IBOutlet weak var emailPudosTextField: PudosTextField!
    
    @IBOutlet weak var passwordPudosTextField: PudosTextField!
    
    
    private let keyboardManager = KeyboardHideManager()
    private var alertManager : AlertViewHandler?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        keyboardManager.targets = [view]
        alertManager = AlertViewHandler(contentStackView: contentView)
        
        self.emailPudosTextField.textField.adjustsFontSizeToFitWidth = true
        self.passwordPudosTextField.textField.adjustsFontSizeToFitWidth = true
        
      

        
    }
    

    @IBAction func didSignInButtonWasPressed(_ sender: Any) {
        
        showSpinner()
        
        guard let email = emailPudosTextField.text else {return}
        guard let password = passwordPudosTextField.text else { return }
        
        
        let errors = EntryValidation(email: email, password: password).validate()
        
        for error in errors {
            
            guard let userInfo = error.userInfo["textField"] as? EntryValidation.TextField else { continue }
            
            switch userInfo {
                
                case .email: emailPudosTextField.show(state: .error(error))
                case .password: passwordPudosTextField.show(state: .error(error))
            
            }
        }
        
        
        PudosService.default.signIn(email: email.noSpace, password: password).then { (response) -> Void  in
            
           if response.result.isSuccessful {
            
                    self.alertManager?.showAlert(message: response.result,color: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
                    guard let token = response.csrf else {return}
                    SessionManager.sharedInstance.register(token: token)
                    RootController.default.initialeViewController()
            
           } else {
            
            if (response.message?.contains("<"))! {
                self.alertManager?.showAlert(message: "Please verify your email using the link we sent you")
            } else {
                self.alertManager?.showAlert(message: response.message ?? "Error")
                
            }
            
           }
    
            }.catch { (error) in
    
                    self.alertManager?.showAlert(message: error.localizedDescription)
                
            }.always {
                
                self.hideSpinner()
        }
        
    }
    
}



