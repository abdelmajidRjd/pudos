//
//  AuthNavigationViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 29/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class AuthNavigationViewController: UINavigationController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        // Do any additional setup after loading the view.
    }

    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CrossDissolveAnimator()
    }
    


}
