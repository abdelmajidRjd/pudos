//
//  RiderSignUpViewController.swift
//  PUDOS
//
//  Created by Majid Inc on 29/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit
import DropDown

class RiderSignUpViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: PudosTextField!
    
    @IBOutlet weak var lastNameTextField: PudosTextField!
    
    @IBOutlet weak var emailTextField: PudosTextField!
    @IBOutlet weak var passwordTextField: PudosTextField!
    // Bith Date
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: PudosTextField!
    
    @IBOutlet weak var inviteTextField: PudosTextField!
    
    @IBOutlet weak var agreementSwitch: UISwitch!
    
    private var alertManager: AlertViewHandler?
    
    @IBOutlet weak var contentView: UIStackView!
    
    private let keyboardManager = KeyboardHideManager()
    
     private let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        keyboardManager.targets = [view]
        
        monthTextField.delegate = self
        
         self.alertManager = AlertViewHandler(contentStackView: contentView)
        
        dropDown.anchorView = monthTextField
        dropDown.dataSource = Helper.instance.month
        dropDown.selectionAction = {  (index: Int, item: String) in
            self.monthTextField.text = String(index + 1)
            self.dropDown.hide()
            
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapSignUpButton(_ sender: Any) {
        
        showSpinner()
        
        guard let firstName = firstNameTextField.text,
            let lastname = lastNameTextField.text,
            let mobile = mobileTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text else {
                //
                return }
        
        
        
        let invite = inviteTextField.text
        let pudosTos = agreementSwitch.isOn ? "yes" : "no"
        
        guard let day = dayTextField.text,
            let month = monthTextField.text,
            let year = yearTextField.text
            else {
                self.alertManager?.showAlert(message: "Birth day not valid !")
                return
                
        }
        
        let bithdate = [year,month.zero,day.zero]
        
     
        PudosService
            .default
            .signUpRider(firstName: firstName.noSpace, lastName: lastname.noSpace
                ,email: email.noSpace, passowrd: password, mobile: mobile.noSpace, birthDate: bithdate.joined(separator: "-"), invite: invite, pudosTos: pudosTos).then { response -> Void  in
                    // redirect
                    
                    if response.result.isSuccessful {
                        self.alertManager?.showAlert(message: response.message ?? "Success, Go back and Sign In",color: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
                        
                        self.performSegue(withIdentifier: "signInUnwindSegue", sender: nil)
                        
                        
                        
                    } else {
                        self.alertManager?.showAlert(message: response.message ?? "Error")
                    }
                     
                    
                    
            }.catch { (error) in
                
                self.alertManager?.showAlert(message: error.localizedDescription)
                
            }.always {
                self.hideSpinner()
        }
        
    }
    
}

extension RiderSignUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        dropDown.show()
    }
    
    
}
extension String {
    var noSpace: String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
