//
//  Extension.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import SwiftyJSON


extension TargetType {
    var plugins : [PluginType] {
        var plugins = [PluginType]()
        plugins.insert(NetworkLoggerPlugin(), at: 0)
        return plugins
    }
}
extension ImmutableMappable {
    
    init(json: JSON) throws {
        try self.init(JSONString: json.description)
    }
    
    static func collection(from json: JSON) throws -> [Self] {
        
        var objects = [Self]()
        
        guard let array = json.array else {
            let _ = try Self.init(json: JSON([:])) // Trigger exception
            return []
        }
        
        for json in array {
            objects.append(try Self.init(json: json))
        }
        return objects
    }
}

// Server Result
extension String {
    
    var isSuccessful: Bool {
        return self == "success"
    }
    
    var isError: Bool {
        return self == "error"
    }
    
    var isWarning: Bool {
        return self == "warning"
    }
    
    var noAuth: Bool {
        return self == "no_auth"
    }
    
}

// Address Parsing
extension String {
    
    
    var zero :String {
        if self.count > 1 {
            return self
        } else {
            return "0"+self
        }
    }
    
    var name: String {
        let name = self.split(separator: ",")[0]
        return String(name)
    }
    
    var state: (state: String,zipCode: String) {
        
        get {
            
            let count = self.split(separator: ",").count
            if (count > 3 ) {
                let stateZip = self.split(separator: ",")[2]
                
                
                    let state = stateZip.split(separator: " ")[0]
                    let zip = stateZip.split(separator: " ")[1]
                    
                    return (String(state),String(zip))
                
            } else {
                
                    let stateZip = self.split(separator: ",")[1]
                    let state = stateZip.split(separator: " ")[1]
                    let zip = stateZip.split(separator: " ")[0]
                
                    return (String(state),String(zip))
                
            }
        }
            

    }
}
