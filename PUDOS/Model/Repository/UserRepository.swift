//
//  UserRepository.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation


struct UserSession {
    let firsname: String
    let lastname: String
    let email: String
    let birthdate: String
    let mobile: String
    let invite: String?
    let image: Data?
}
protocol UserRepo {
    func save(firstname: String, lastname: String, email: String, birthDate: String, mobile: String, invite: String?, image: Data?)
    func getUser() -> UserSession?
    func save(pic: String)
    func getPicUrl() -> URL?
}


struct UserRepository : UserRepo {
    
    
    
    func save(pic: String) {
        UserDefaults.standard.set(pic, forKey: ProfileKey.picUrl.rawValue)
    }
    
    func getPicUrl() -> URL? {
        guard let url  = UserDefaults.standard.string(forKey: ProfileKey.picUrl.rawValue) else {return nil}
        return URL(string: url)
    }
    
    
    func getUser() -> UserSession? {
    
        guard let email  = UserDefaults.standard.string(forKey: ProfileKey.email.rawValue) else {return nil}
        guard let firstname  = UserDefaults.standard.string(forKey: ProfileKey.firstname.rawValue) else {return nil}
        guard let lastname  = UserDefaults.standard.string(forKey: ProfileKey.lastname.rawValue) else {return nil}
        guard let mobile  = UserDefaults.standard.string(forKey: ProfileKey.mobile.rawValue) else {return nil}
        guard let birthdate  = UserDefaults.standard.string(forKey: ProfileKey.birthdate.rawValue) else {return nil}
        
        let image = UserDefaults.standard.data(forKey: ProfileKey.image.rawValue)
        
        let invite  = UserDefaults.standard.string(forKey: ProfileKey.invite.rawValue)
        
        let user = UserSession(firsname: firstname, lastname: lastname, email: email, birthdate: birthdate, mobile: mobile, invite: invite,image: image)
        return user
        
        
    }
    
    
    
    func save(firstname: String, lastname: String, email: String, birthDate: String, mobile: String, invite: String?, image: Data?) {
        
        UserDefaults.standard.set(firstname, forKey: ProfileKey.firstname.rawValue)
        UserDefaults.standard.set(lastname, forKey: ProfileKey.lastname.rawValue)
        UserDefaults.standard.set(email, forKey: ProfileKey.email.rawValue)
        UserDefaults.standard.set(mobile, forKey: ProfileKey.mobile.rawValue)
        UserDefaults.standard.set(birthDate, forKey: ProfileKey.birthdate.rawValue)
        UserDefaults.standard.set(invite, forKey: ProfileKey.invite.rawValue)
        UserDefaults.standard.set(invite, forKey: ProfileKey.invite.rawValue)
        UserDefaults.standard.set(image, forKey: ProfileKey.image.rawValue)
    }
    
    
    static let `default` = UserRepository()
    private init(){}
    
    
   
    
    
}
 enum ProfileKey: String {
    case firstname = "firstname"
    case lastname = "lastname"
    case email = "email"
    case birthdate = "birthdate"
    case mobile = "mobile"
    case invite = "invite"
    case image = "image"
    case picUrl = "pic"
}
