//
//  PudosRepository.swift
//  PUDOS
//
//  Created by Majid Inc on 06/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation

struct PudosRepository {
    
    static var deliverySpeed : [String:Int] {
        get {
            return ["Need it now":1, "Next within an hour":2,"Within an 6 hours":3, "Within 12 hours":4, "Within a Day":5, "Within a week":6]
        }
    }
    
    static var delivery : [String] {
        get {
            return ["Need it now", "Next within an hour","Within an 6 hours", "Within 12 hours", "Within a Day", "Within a week"]
        }
    }
    
    static func method(reim: Int) -> String {
        if ( reim == 1 ){
            return "Cash"
        }else {
            return "Venmo"
        }
    }
    
    static var reimbouresement : [String:Int] {
        get {
            return ["Cash":1,"Venmo":4]
        }
    }
    static var reimb : [String] {
        get {
            return ["Cash","Venmo"]
        }
    }
    static var status: [String: String] {
        return ["Open": "open", "Confirmed": "2", "En Route":"4", "Completed": "done", "Cancelled": "canceled"]
    }
    static var search: [String] {
        return ["Open", "Completed","Cancelled"]
    }
}

