//
//  User.swift
//  PUDOS
//
//  Created by Majid Inc on 31/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper


class Referrer: ImmutableMappable {
    let email: String
    let firstName: String
    let lastName: String
    required init(map: Map) throws {
        self.email = try map.value("email")
        self.firstName = try map.value("first_name")
        self.lastName = try map.value("last_name")
    }
}



class DateInfo: ImmutableMappable {
    
    let date: String
    let timezoneType: Int
    let timezone: String
    
    required init(map: Map) throws {
        self.date = try map.value("date")
        self.timezoneType = try map.value("timezone_type")
        self.timezone = try map.value("timezone")
    }
    
}


class Notif: ImmutableMappable {
    let maxDisplay: Int
    let pollRate: Int
    
    required init(map: Map) throws {
        self.pollRate = try map.value("pollRate")
        self.maxDisplay = try map.value("maxDisplay")
    }

}

class Setting: ImmutableMappable {
    let unitSystem: String
    let showTutorial: Bool
    let notifications: Notif
    
    required init(map: Map) throws {
        
        self.unitSystem = try map.value("unitSystem")
        self.showTutorial = try map.value("showTutorial")
        self.notifications = try map.value("notifications")
    }
    
    
}

class User : ImmutableMappable {
    let id: String
    let idStripe: String
    let email: String
    let firstName: String
    let lastName: String
    let birthDate: DateInfo
    let mobile: String
    let referralCode: String // Referral_code
    let referrer: Referrer?
    let addressLine1: String?
    let addressLine2: String?
    let city: String?
    let state: String?
    let zipcode: String?
    let picture: String
    let credit: String
    let type: Int
    let status: Int
    let statusInfo: String?
    let rating: Int
    let settings: Setting
    let created: DateInfo
    let modified: DateInfo
    
    required init(map: Map) throws {
        
        self.id = try map.value("id")
        self.idStripe = try map.value("id_stripe")
        self.email = try map.value("email")
        self.firstName = try map.value("first_name")
        self.lastName = try map.value("last_name")
        self.birthDate = try map.value("birth_date")
        
        self.mobile = try map.value("mobile")
        self.referralCode = try map.value("referral_code")
        self.referrer = try? map.value("referrer")
        self.addressLine1 = try? map.value("address_line_1")
        self.addressLine2 = try? map.value("address_line_2")
        self.city = try? map.value("city")
        self.state = try? map.value("state")
        self.zipcode = try? map.value("zipcode")
        self.picture = try map.value("picture")
        self.credit = try map.value("credit")
        self.type = try map.value("type")
        self.status = try map.value("status")
        self.statusInfo = try? map.value("status_info")
        self.rating = try map.value("rating")
        self.created = try map.value("created")
        self.modified = try map.value("modified")
        self.settings = try map.value("settings")
        
        
        
    }
}
class PudosProfile:  Pudos {
    let profile: User?
    
    
    
    required init(map: Map) throws {

        self.profile = try? map.value("profile")
        try super.init(map: map)
    }
}

