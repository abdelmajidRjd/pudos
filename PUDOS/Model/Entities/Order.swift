//
//  Order.swift
//  PUDOS
//
//  Created by Majid Inc on 04/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON


class WayPoints: ImmutableMappable {
    
    let data : [WayPoint]
    
    func mapping(map: Map) {
        data >>> map["data"]
    }
    
    init(waypoints: [WayPoint]) {
        self.data = waypoints
    }
    
    required init(map: Map) throws {
        self.data = try map.value("data")
    }
}
class Location: ImmutableMappable {
    
    let lat: Double
    let lng: Double
    
    init(lat: Double, lng: Double) {
        self.lng = lng
        self.lat = lat
    }
    
    func mapping(map: Map) {
        lat >>> map["lat"]
        lng >>> map["lng"]
    }
    
    required init(map: Map) throws {
        self.lat = try map.value("lat")
        self.lng = try map.value("lng")
    }
    
}
class WayPoint: ImmutableMappable {
    
    let name: String
    let address: String
    let state: String
    let zipCode: String
    let description: String?
    let type: Int
    let data: String?
    let regul: Bool?
    let location: Location
    
    
    init(name: String,address: String, state: String, zipCode: String, description: String?, type: Int, data: String?, regul: Bool?, location: Location) {
        
        self.name = name
        self.address = address
        self.state = state
        self.zipCode = zipCode
        self.description = description
        self.type = type
        self.data = data
        self.regul = regul
        self.location = location
    }
 
    
    func mapping(map: Map) {
        name >>> map["name"]
        address >>> map["addr"]
        state >>> map["state"]
        zipCode >>> map["zipcode"]
        description >>> map["desc"]
        type >>> map["type"]
        data >>> map["data"]
        regul >>> map["regul"]
        location >>> map["location"]
        
    }
    
    required init(map: Map) throws {
        
        self.name = try map.value("name")
        self.address = try map.value("addr")
        self.state = try map.value("state")
        self.zipCode = try map.value("zipcode")
        self.description = try map.value("desc")
        self.type = try map.value("type")
        self.data = try map.value("data")
        self.regul = try map.value("regul")
        self.location = try map.value("location")
    }
    
}

class Pudos : ImmutableMappable {
    let result: String
    let message: String?
    let delete: String?
    
    required init(map: Map) throws {
        self.result = try map.value("result")
        self.message = try? map.value("message")
        self.delete = try? map.value("delete")
    }
}

class CancelPudosResponse: Pudos {
    
    required init(map: Map) throws {
        self.orderId = try? map.value("orderId")
        self.refundError = try? map.value("refundError")
        try super.init(map: map)
    }
    
    let orderId: String?
    let refundError: Bool?
}


class OrderConfirmationResponse : ImmutableMappable {
    
    required init(map: Map) throws {
        self.result = try map.value("result")
        self.message = try? map.value("message")
        self.orderId = try? map.value("orderId")
        self.method = try? map.value("method")
    }
    let result: String
    let message: String?
    let orderId: String?
    let method: String?
}



// MARK: - Order Map :

class OrderMapResponse: ImmutableMappable {
    
    required init(map: Map) throws {
      
        self.id = try map.value("id")
        self.distance = try map.value("distance")
        self.duration = try map.value("duration")
        self.directions = try? map.value("directions")
        self.price = try map.value("price")
        self.base = try map.value("base")
        self.credit = try map.value("credit")
        self.reimb = try map.value("reimb")
        self.coupon = try? map.value("coupon")
        self.tax = try map.value("tax")
        self.nin = try map.value("nin")
    }
  
    
    
    let id: String
    let distance: Float
    let directions: String?
    let duration: Float
    let price: Float
    let base: Float
    let credit: Float
    let reimb: String
    let coupon: Int?
    let tax: Float
    let nin: Bool
}

class Credit: ImmutableMappable {
    
    required init(map: Map) throws {
        self.credit = try map.value("credit")
    }
    let credit: String
}

//- MARK:  Create Order Confirmation
class PudosOrderCreationResponse: Pudos {
    
    required init(map: Map) throws {
        self.order = try? map.value("order")
        self.user = try? map.value("user")
        try super.init(map: map)
    }
    let order: OrderMapResponse?
    let user: Credit?
}

class Order: ImmutableMappable {
    
    required init(map: Map) throws {
        self.id = try map.value("id")
        self.status = try map.value("status")
        self.statusStr = try map.value("status_str")
        self.created = try map.value("created")
    }
    
    let id: String
    let status:Int
    let statusStr: String
    let created: String
    
    func isOpen() -> Bool{ return self.status == 1 }
    func isConfirmed() -> Bool{ return self.status == 2 }
    func isEnRoute() -> Bool{ return self.status == 4 }
    func isCompleted() -> Bool{ return self.status == 16 }
    func isCancelled() -> Bool{ return self.status == 32 }
    
}
/**
 //
 "1" for Open
 "2" for Confirmed
 "4" for En route
 "16" for Completed
 "32" for Cancelled
 **/
// find Order
class PudosOrderResponse: Pudos {

    required init(map: Map) throws {
        self.orders = try? map.value("orders")
        self.totalCount = try? map.value("totalCount")
        try super.init(map: map)
        
    }
    let totalCount: String?
    let orders: [Order]?
}

// MARK: - Detail Order
class OrderDetailResponse: Pudos {
    
    let details :OrderDetail?
    
    required init(map: Map) throws {
        
        self.details = try? map.value("details")
        try super.init(map: map)
    }  
}

class LocationInfo: ImmutableMappable {
    let lat: Float
    let lng: Float
    required init(map: Map) throws {
        self.lat = try map.value("lat")
        self.lng = try map.value("lng")
    }
    
}
class Request: ImmutableMappable {
    let source: LocationInfo
    let destination: LocationInfo
    
    required init(map: Map) throws {
        
        self.source = try map.value("origin")
        self.destination = try map.value("destination")
    }
}

class Direction: ImmutableMappable {
    let request: Request
    required init(map: Map) throws {
        self.request = try map.value("request")
    }    
}




class OrderDetail: ImmutableMappable {
    
    let id: String
    let riderId: String
    let instructions: String
    let deliverySpeed: String
    let directions: String?
    let distance: Int
    let tolls: String?
    let reimbAmount: String?
    let reimbConfirm: Bool
    let status: Bool
    let created: DateInfo
    let modified: DateInfo
    
    
    required init(map: Map) throws {
        
        self.id = try map.value("order_id")
        self.riderId = try map.value("rider_id")
        self.instructions = try map.value("instructions")
        self.deliverySpeed = try map.value("delivery_speed")
        self.directions = try? map.value("directions")
        self.distance = try map.value("distance")
        self.tolls = try? map.value("tolls")
        self.reimbAmount = try? map.value("reimb_amount")
        self.reimbConfirm = try map.value("reimb_confirm")
        self.status = try map.value("status")
        self.created = try map.value("created")
        self.modified = try map.value("modified")
    }
  
}
