//
//  Card.swift
//  PUDOS
//
//  Created by Majid Inc on 02/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper


class Card: ImmutableMappable {
    
    required init(map: Map) throws {
        self.id = try map.value("id")
        self.brand = try map.value("brand")
        self.exp = try map.value("exp")
        self.last4 = try map.value("last4")
        self.current = try map.value("default")
 
    }
    
    let id: String
    let brand: String
    let exp: String
    let last4: String
    let current: Bool
    
}

class PudosCardResponse: Pudos {
    
    required init(map: Map) throws {
        self.cards = try? map.value("cards")
        try super.init(map: map)
    }
    
    let cards: [Card]?
}
