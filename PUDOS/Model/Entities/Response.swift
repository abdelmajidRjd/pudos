//
//  Success.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper


class PudosResponse: Pudos {
    
    let csrf: String?
    let pic: String?
    
    required init(map: Map) throws {
        self.csrf = try? map.value("csrf")
        self.pic = try? map.value("new_pic")
        try super.init(map: map)
    }
    
}



