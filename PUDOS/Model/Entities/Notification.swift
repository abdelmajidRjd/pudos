//
//  Notification.swift
//  PUDOS
//
//  Created by Majid Inc on 01/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationData: ImmutableMappable {
  
    required init(map: Map) throws {
        self.id = try map.value("id")
        self.objectId = try map.value("objectId")
        self.message = try map.value("message")
        self.type = try map.value("type")
        self.created = try map.value("created")
    }
    
    let id: String
    let type: Int
    let objectId: String
    let created: String
    let message:String
    
}

class PudosNotification: Pudos {
    
    
    let count: Int?
    let total: String?
    let data: [NotificationData]?
    
    required init(map: Map) throws {
        self.count = try? map.value("count")
        self.total = try? map.value("total")
        self.data = try? map.value("data")
        try super.init(map: map)
    }
}
