//
//  Session.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation

class Session: NSObject, NSSecureCoding{
    
    private var _token = ""
    
    
    var token : String {
        
        get{
            return _token
        }
        set{
            _token = newValue
        }
    }
    
    override init() {}
    
    init(token: String) {
        self._token = token
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let authToken = aDecoder.decodeObject(forKey: SessionKeys.token) as? String{
            _token = authToken
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_token, forKey : SessionKeys.token)
    }
    
    // static var supportsSecureCoding = true
    private struct SessionKeys {
        static var token = "token"
    }
    public static var supportsSecureCoding = true
}
