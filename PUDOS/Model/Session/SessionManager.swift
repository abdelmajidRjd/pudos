//
//  SessionManager.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation

fileprivate protocol SessionManagerProtocol {
    
    func register(token: String)
    func invalidate()
    func save(cookie : HTTPCookie)
    
    
}
struct SessionManager : SessionManagerProtocol {
   
    
  
    
    
    private static let userKey = "UserInfoKey"
    
   
    
    static let sharedInstance = SessionManager()
    private init(){}
    
    func register(token: String) {
        let session = Session(token: token)
        let data = NSKeyedArchiver.archivedData(withRootObject : session)
        UserDefaults.standard.set(data, forKey: SessionManager.userKey)  // to refactor
    }
    
    func invalidate() {
        UserDefaults.standard.removeObject(forKey: SessionManager.userKey)
        UserDefaults.standard.removeObject(forKey: SessionManager.cookieKey)
    }
    
    func token() -> String? {
        guard let data = UserDefaults.standard.data(forKey: SessionManager.userKey),
            let session = NSKeyedUnarchiver.unarchiveObject(with: data) as? Session
            else{return nil}
        return session.token
    }
    
    static var current: Session? {
        get {
            guard let data = UserDefaults.standard.data(forKey: userKey),
                let session = NSKeyedUnarchiver.unarchiveObject(with: data) as? Session
                else { return nil }
            
            return session
            
        }
        
    }
    static var cookie: HTTPCookie? {
        get {
            guard let data = UserDefaults.standard.data(forKey: cookieKey),
                let cookie = NSKeyedUnarchiver.unarchiveObject(with: data) as? HTTPCookie
                else { return nil }
            
            return cookie
            
        }
        
    }
    
    
    private static let cookieKey = "CookieKey"
    
    func save(cookie: HTTPCookie) {
        let data = NSKeyedArchiver.archivedData(withRootObject : cookie)
        UserDefaults.standard.set(data, forKey: SessionManager.cookieKey)
    }
    
}


