//
//  Config.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Config {
    
    fileprivate let data: JSON
    
    // MARK: - Init Method
    init() {
        let filePath = Bundle.main.path(forResource: "Config", ofType: "plist") ?? ""
        let data = NSDictionary(contentsOfFile: filePath) ?? [:]
        let json = JSON(data)
        self.data = json
    }
}
extension Config {
    
    var baseURL: URL {
        return URL(string: data["baseURL"].stringValue)!
    }
    
    var publisherKey: String {
        return data["publisherKey"].stringValue
    }
    
}

