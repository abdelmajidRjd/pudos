//
//  Validation.swift
//  PUDOS
//
//  Created by Majid Inc on 29/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation



struct EntryValidation {
    
    private let email: String
    private let password: String
    
    
    init(email: String, password: String){
        self.email = email
        self.password = password
    }
    
    
    func validate(email: String?) -> Bool{
        guard let email = email else { return false }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func validate(password: String?) -> Bool{
        guard let password = password else { return false }
        let passwordRegEx = "[A-Za-z0-9]{6,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
    
    
    func error(field: TextField, reason: String) -> NSError {
        
        return NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey:reason,"textField":field])
    }
    
    func validate() -> [NSError]{
        
        var errors: [NSError] = []
        
        if !validate(email:email) {
            errors.append(error(field: .email, reason: "invalid email"))
        }
        
        if !validate(password: password){
            errors.append(error(field: .password, reason: "You must enter at least 6 digits"))
        }
        
        return errors
    }
    
    enum TextField{
        case email,password
    }
    
}
