//
//  Helper.swift
//  PUDOS
//
//  Created by Majid Inc on 30/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation

protocol BirthDate {
    var month : [String] { get }
}
struct Helper : BirthDate {
    
    static let instance = Helper()
    private init(){}
    
    var month: [String] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
}
struct PudosHtmlParser {
    
    // deprecated : I Should use slicing subscript
    static func pudosId(message: String) -> String? {
        if let test = message.range(of: "(?<=id=)[^\"]+", options: .regularExpression) {
             //return message.substring(with: test)
             return String(message[test])
        } else {
            return nil
        }
    }
    static func message(message: String) -> String {
        let textMessage = message.split(separator: "<")[0]
        return String(textMessage)
        
    }
    
}

