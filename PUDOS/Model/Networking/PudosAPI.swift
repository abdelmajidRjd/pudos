//
//  PudosAPI.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON



enum PudosAPI {
    
    // Sign Up
    case signUpRider(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?, pudosTos: String)
    
    case signUpDriver(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?,firstAddress: String, secondAddress:String?,city: String, zipcode: String, state: String, carBrand: String, carModel: String, carYear: Int, carType: String, carImage: Data, carInsurance: String, driverLicencse: String, ssnToken: String, bankToken: String, venmo: String, stripeTos: String, pudosTos: String)
    
    
    case check(email: String)
    case checkInviteCode(userType: String, invite: String)
    
    // login
    case signIn(email: String, password: String)
    case resetPassword(email: String, birthDate: String)
    
    
    // Notification
    case getRecentNotification
    case getAllNotification
    case dismissOne(id: String)
    case dismissAll
    
    // user
    case profile
    
    case updateProfile(firstName: String
        , lastName: String, mobile: String, birthDate: String, oldPassword: String, newPassword : String
        ,confirmPassword: String, file: Data?, pictureAction: String, invite: String)
    
    case getCreditCards
    case addCreditCard(stripToken: String)
    case removeCreditCard(stripToken: String)
    case setDefaultSource(stripToken: String)
    case sendInviteEmail(email:String)
    case getDefaultInviteCode
    case getReferralStatistics(start: Date, end: Date)
    case setShowTutoriel(show: String)
    
    
    //order

    case createOrder(wayPoints: String,instructions: String, reimb:Int, speed: Int,itemImages: [Data], promo: String?, optimize: Bool)
    
    case confirmOrder(method: String,orderId: String)
    case findOrder(id: String?, status: String?, start: String?, end: String?, page: Int)
    case cancelOrder(id: String)
    case orderDetail(id: String)
    
    
    // Pick ups
    case getAvailablePickUps(latitute: Float, longitude: Float)
    case getPickUpSummary(id: String)
    
}

extension PudosAPI: TargetType {
    
    
    
    
    var baseURL: URL { return Config().baseURL }
    
    var path: String {
        
        
        switch self {
            case .signUpRider,
             .signUpDriver,
             .check,
             .checkInviteCode: return "/signup.php"
            
            case .signIn,
                 .resetPassword : return "/login.php"
            
            case .getAllNotification,
                 .getRecentNotification,
                 .dismissAll,
                 .dismissOne: return "/notifications.php"
            
            
            case  .profile,
             .updateProfile,
             .getCreditCards,
             .addCreditCard,
             .removeCreditCard,
             .setDefaultSource,
             .sendInviteEmail,
             .getDefaultInviteCode,
             .getReferralStatistics,
             .setShowTutoriel: return "/user.php"
       
            
            
            case .createOrder,
             .confirmOrder,
             .findOrder,
             .cancelOrder,
             .orderDetail:
                return "/rider/order.php"
            
            case .getAvailablePickUps,
             .getPickUpSummary: return  "/driver/pickup.php"
            
        }
        
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var sampleData: Data {
        /*
         guard let url = Bundle.main.url(forResource: "available_producers", withExtension: "json") else { return Data()}
         return try! Data(contentsOf: url, options: .alwaysMapped)
 
 */
 
        switch self {
            
            case .signUpRider,
                .signUpDriver,
                .resetPassword,
                .dismissAll,
                .dismissOne:
                
                guard let url = Bundle.main.url(forResource: "success", withExtension: "json") else { return Data()}
                
                return try! Data(contentsOf: url, options: .alwaysMapped)
            
            case .signIn:
                
                guard let url = Bundle.main.url(forResource: "login", withExtension: "json") else { return Data()}
                return try! Data(contentsOf: url, options: .alwaysMapped)
            
            case .profile:
                
                guard let url = Bundle.main.url(forResource: "profile", withExtension: "json") else { return Data()}
                return try! Data(contentsOf: url, options: .alwaysMapped)
        case .updateProfile : return Stub.data(from: "update_profile", withExtension: "json")
            
        case .getAllNotification, .getRecentNotification:
            
            guard let url = Bundle.main.url(forResource: "notification", withExtension: "json") else { return Data()}
            return try! Data(contentsOf: url, options: .alwaysMapped)
        case .addCreditCard, .getCreditCards :
             return Stub.data(from: "credit_cards", withExtension: "json")
        case .createOrder :
             return Stub.data(from: "order", withExtension: "json")
            default :
                return Data()
        }
        
        
        
    }
   
    
    var task: Task {
        if let params = parameters {
            
            return .requestParameters(parameters: params, encoding: encoding)
        }
        return .requestPlain
    }
    
    var headers: [String: String]? {
        switch self {
        case .updateProfile, .profile:
            
            guard let value = cookie else { return nil }
            
            return ["Cookie":value]
        default:
            return nil
        }
        
    }
    
    var validate: Bool {
        return true
    }
    
    // MARK: Internals
    
    private var encoding: ParameterEncoding {
        
        
            return URLEncoding.default
    
        /*switch self {
         case .getAvailableProducers:  return JSONEncoding.default
         default: return URLEncoding.default
         }*/
    }
    
    private var parameters: [String: Any]? {
        
        switch self {
            
            
            // Sign Up
        case .signUpRider(let firstName, let lastName, let email, let passowrd, let mobile, let birthDate, let invite, let pudosTos):
            if let invite = invite {
                return ["action": "signup","user-type": "r", "first-name": firstName, "last-name": lastName, "email": email, "mobile": mobile, "birth-date": birthDate, "invite": invite, "pudos-tos": pudosTos, "password":passowrd]
            } else {
                return ["action": "signup","user-type": "r", "first-name": firstName, "last-name": lastName, "email": email, "mobile": mobile, "birth-date": birthDate, "pudos-tos": pudosTos, "passowrd":passowrd]
            }
            
       
        case .signUpDriver(let firstName,let lastName, let email,let passowrd, let mobile, let birthDate,let invite, let firstAddress, let secondAddress,let city,let zipcode,let state, let carBrand, let carModel, let carYear, let carType,let carImage,let carInsurance,let driverLicencse,let ssnToken,let bankToken,let venmo,let stripeTos, let pudosTos):
            

            return ["action": "signup","user-type": "d", "first-name": firstName, "last-name": lastName, "email": email, "mobile": mobile, "birth-date": birthDate, "invite": invite ?? "", "pudos-tos": pudosTos, "passowrd":passowrd, "addr-1":firstAddress,"addr-2": secondAddress ?? "", "city":city, "zipcode": zipcode, "state": state, "car_brand": carBrand, "car_model": carModel, "car_year": carYear, "car_type": carType, "car_image": carImage, "car_insurance": carInsurance,"drivers-license": driverLicencse, "ssn-token":ssnToken, "ba-token": bankToken, "venmo":venmo, "pudos_tos": pudosTos, "stripe_tos": stripeTos]
            
            
        case .check(let email):
            return ["action": "email_check","email": email]
            
        case .checkInviteCode(let userType, let invite):
            return ["action": "invite_check", "user-type": userType, "invite": invite]
            
            
            
        // Login
        case .signIn(let email, let password):
            return ["action": "login","email": email,"password":password]
            
        case .resetPassword(let email,let birthDate):
            return ["action":"password-reset", "email": email, "birth-date": birthDate]
            
            
         // notification
        case .getAllNotification:
            return ["action": "get_all","csrf-token": token ?? ""]
        case .getRecentNotification:
            return ["action": "get_recent","csrf-token": token ?? ""]
        case .dismissOne(let id):
            return ["action": "dismiss","csrf-token": token ?? "","id":id]
        case .dismissAll:
            return ["action": "dismiss_all","csrf-token": token ?? ""]
            
            
            
        case .profile:
             let data = ["action": "get-profile","csrf-token":token ?? ""]
             
             return data
        
        case .updateProfile( let firstName, let lastName, let mobile , let birthDate,let oldPassword, let newPassword,let confirmPassword, let file, let pictureAction, let invite):
            return ["action": "profile-update", "csrf-token": token ?? "", "first-name" : firstName, "last-name": lastName, "mobile": mobile, "birth-date": birthDate, "password-old": oldPassword, "password-new": newPassword, "password-confirm": confirmPassword, "profile-file": file ?? Date(), "profile-picture-action": pictureAction, "invite": invite]
        
        case .getCreditCards:
            return ["action": "list-cards", "csrf-token": token ?? ""]
            
        case .addCreditCard(let stripToken):
            
           return ["action": "add-card", "csrf-token": token ?? "", "token": stripToken]
        case .removeCreditCard(let stripToken):
            
            return ["action": "delete-card","csrf-token": token ?? "", "token": stripToken]
        case .setDefaultSource(let stripToken):
            
            return ["action": "make-card-default","csrf-token": token ?? "", "token": stripToken]
        case .sendInviteEmail(let email):
            
            return ["action": "send-invite","csrf-token": token ?? "", "email": email]
        case .getDefaultInviteCode:
            
            return ["action": "default-invite-code","csrf-token": token ?? ""]
        case .getReferralStatistics(let start,let end):
            
            return ["action": "referrals-stats","csrf-token": token ?? "","dstart":start, "dend":end]
        case .setShowTutoriel(let show):
            
            return ["action": "show-tutorial","csrf-token": token ?? "","show":show]
            
            // Order
            
        case .createOrder( let wayPoints, let instructions,let reimb, let speed,let itemImages,let promo, let optimize):
            if let promo = promo {
                  return ["action":"create","csrf-token": token ?? "", "waypoints": wayPoints, "instructions":instructions, "reimb": reimb, "speed":speed, "itemImages": itemImages, "promo": promo, "optimize": optimize]
                
            } else {
                  return ["action":"create","csrf-token": token ?? "", "waypoints": wayPoints, "instructions":instructions, "reimb": reimb, "speed":speed, "itemImages": itemImages, "optimize": optimize]
                
            }
          
            
        case .confirmOrder(let method,let orderId):
            return ["action": "confirm","csrf-token": token ?? "","method":method,"id":orderId]
        case .findOrder(let id, let status, let start,let end,let page):
            
            
            
            return ["action": "find","csrf-token": token ?? "","id": id ?? "","status":status ?? "", "start":start ?? "", "end":end ?? "", "page": page]
        
        case .cancelOrder(let id):
             return ["action": "cancel","csrf-token": token ?? "","id":id]
            
        case .orderDetail(let id) :
            return ["action": "get", "csrf-token": token ?? "","id": id]
            
        
        case .getAvailablePickUps(let latitute,let longitude):
            return ["action": "getNearByPickups", "csrf-token": token ?? "", "lat": latitute, "lng": longitude]
        case .getPickUpSummary(let id):
            return ["action": "getPickupShortInfo", "csrf-token": token ?? "", "id": id]
            
        }
        
        
    }
    
    
    
    /*
     var multipartBody: [MultipartFormData]? {
     switch self {
     case .PostTest(let multipartData):
     
     guard let image = multipartData["image"] as? [NSData] else { return[] }
     
     let formData: [MultipartFormData] = image.map{MultipartFormData(provider: .Data($0), name: "images", mimeType: "image/jpeg", fileName: "photo.jpg")}
     return formData
     
     
     default:
     return []
     }
     }
     */
    
}
