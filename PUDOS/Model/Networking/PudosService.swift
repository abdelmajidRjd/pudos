//
//  PudosService.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyJSON

protocol PudosAuthStore {
    
    func signUpRider(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?, pudosTos: String) -> Promise<PudosResponse>
    
    
    func signUpDriver(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?,firstAddress: String, secondAddress:String?,city: String, zipcode: String, state: String, carBrand: String, carModel: String, carYear: Int, carType: String, carImage: Data, carInsurance: String, driverLicencse: String, ssnToken: String, bankToken: String, venmo: String, stripeTos: String, pudosTos: String) -> Promise<PudosResponse>
    
    func signIn(email: String, password: String) ->  Promise<PudosResponse>
    func resetPassword(email: String, birthDate: String) -> Promise<PudosResponse>
    
    
}

protocol PudosUserStore {
    
    func profile() -> Promise<PudosProfile>
    
    func updateProfile(firstName: String
        , lastName: String, mobile: String, birthDate: String, oldPassword: String, newPassword : String
        ,confirmPassword: String, file: Data?, pictureAction: String, invite: String) -> Promise<PudosResponse>
    
    func getCreditCards() -> Promise<PudosCardResponse>
    
    func addCreditCard(stripToken: String) -> Promise<PudosCardResponse>
    
    func removeCreditCard(stripToken: String) -> Promise<PudosCardResponse>
    
    func sendInviteEmail(email:String) -> Promise<Pudos>
    
}

protocol PudosNotificationStore {
    
    func getRecentNotification() -> Promise<PudosNotification>
    func getAllNotification() -> Promise<PudosNotification>
    func dismissOne(id: String) -> Promise<Pudos>
    func dismissAll() -> Promise<Pudos>
    
}


protocol PudosDriverOrderStore {
    func findOrder(id: String?, status: String?, start: String?, end: String?, page: Int) ->Promise<PudosOrderResponse>
    
     func createOrder(wayPoints: String,instructions: String, reimb:Int, speed: Int,itemImages: [Data], promo: String?, optimize: Bool) -> Promise<PudosOrderCreationResponse>
    func confirmOrder(method: String,orderId: String) -> Promise<OrderConfirmationResponse>
    
    func cancelOrder(id: String) -> Promise<CancelPudosResponse>
    
    func orderDetail(id: String) -> Promise<OrderDetailResponse>
}

struct PudosService: PudosAuthStore {
    
    
    static let `default` =  PudosService()
    private init(){}
    
    
    
    func signUpRider(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?, pudosTos: String) -> Promise<PudosResponse> {
        
        let promise =   NetworkManager.request(target: .signUpRider(firstName: firstName, lastName: lastName, email: email, passowrd: passowrd, mobile: mobile, birthDate: birthDate, invite: invite, pudosTos: pudosTos))
        
        return promise.then(execute: { (response) -> Promise<PudosResponse> in
            return Promise<PudosResponse>(value: try PudosResponse(json: response))
        })
    }
    
    func signUpDriver(firstName: String, lastName: String, email: String, passowrd: String, mobile: String, birthDate: String, invite: String?, firstAddress: String, secondAddress: String?, city: String, zipcode: String, state: String, carBrand: String, carModel: String, carYear: Int, carType: String, carImage: Data, carInsurance: String, driverLicencse: String, ssnToken: String, bankToken: String, venmo: String, stripeTos: String, pudosTos: String) -> Promise<PudosResponse> {
        
        let promise = NetworkManager.request(target: .signUpDriver(firstName: firstName, lastName: lastName, email: email, passowrd: passowrd, mobile: mobile, birthDate: birthDate, invite: invite, firstAddress: firstAddress, secondAddress: secondAddress, city: city, zipcode: zipcode, state: state, carBrand: carBrand, carModel: carModel, carYear: carYear, carType: carType, carImage: carImage, carInsurance: carInsurance, driverLicencse: driverLicencse, ssnToken: ssnToken, bankToken: bankToken, venmo: venmo, stripeTos: stripeTos, pudosTos: pudosTos))
        
        return promise.then(execute: { (response) ->  Promise<PudosResponse> in
            return Promise<PudosResponse>(value: try PudosResponse(json: response))
        })
        
    }
    
    
    
    
    func signIn(email: String, password: String) -> Promise<PudosResponse> {
        
        let promise = NetworkManager.request(target: .signIn(email: email, password: password))
        
        
        return promise.then(execute: { (response) -> Promise<PudosResponse> in
            return Promise<PudosResponse>(value: try PudosResponse(json: response))
        })
        
    }
    
    func resetPassword(email: String, birthDate: String) -> Promise<PudosResponse> {
         let promise = NetworkManager.request(target: .resetPassword(email: email, birthDate: birthDate))
        
        return promise.then(execute: { (response) -> Promise<PudosResponse> in
            return Promise<PudosResponse>(value: try PudosResponse(json: response))
        })
        
    }
    
    
    
}

extension PudosService: PudosUserStore {
    
    
    func sendInviteEmail(email: String) -> Promise<Pudos> {
        
        let promise = NetworkManager.request(target: .sendInviteEmail(email: email))
        return promise.then(execute: { (json) -> Promise<Pudos> in
            return Promise<Pudos>(value: try Pudos(json: json))
        })
        
    }
    
    
    
    func removeCreditCard(stripToken: String) -> Promise<PudosCardResponse> {
        
        let promise = NetworkManager.request(target: .removeCreditCard(stripToken: stripToken))
        
        return promise.then(execute: { (response) -> Promise<PudosCardResponse> in
            return Promise<PudosCardResponse>(value: try PudosCardResponse(json: response))
        })
        
    }
    
    
 
    
    func addCreditCard(stripToken: String) -> Promise<PudosCardResponse> {
        let promise = NetworkManager.request(target: .addCreditCard(stripToken: stripToken))
        
        return promise.then(execute: { (response) -> Promise<PudosCardResponse> in
            return Promise<PudosCardResponse>(value: try PudosCardResponse(json: response))
        })
        
    }
    
    
   
    
    func updateProfile(firstName: String, lastName: String, mobile: String, birthDate: String, oldPassword: String, newPassword: String, confirmPassword: String, file: Data?, pictureAction: String, invite: String) -> Promise<PudosResponse> {
        
        let promise = NetworkManager.request(target: .updateProfile(firstName: firstName, lastName: lastName, mobile: mobile, birthDate: birthDate, oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword, file: file, pictureAction: pictureAction, invite: invite))
        
        
        return promise.then(execute: { (response) -> Promise<PudosResponse> in
            return Promise<PudosResponse>(value: try PudosResponse(json: response))
        })
    }
    
    func profile() -> Promise<PudosProfile> {
         let promise = NetworkManager.request(target: .profile)
        
        return promise.then(execute: { (response) -> Promise<PudosProfile> in
            
             return Promise<PudosProfile>(value: try PudosProfile(json: response))
        })
        
    }
    
    
    func getCreditCards() -> Promise<PudosCardResponse> {
        
        let promise = NetworkManager.request(target: .getCreditCards)
        
        return promise.then(execute: { (response) -> Promise<PudosCardResponse> in
            return Promise<PudosCardResponse>(value: try PudosCardResponse(json: response))
        })
        
    }
    
    
    
    
}
extension PudosService: PudosNotificationStore {
    
    func dismissOne(id: String) -> Promise<Pudos> {
        let promise = NetworkManager.request(target: .dismissOne(id: id))
        return promise.then(execute: { (response) -> Promise<Pudos> in
            return Promise<Pudos>(value: try Pudos(json: response))
        })
    }
    
    func dismissAll() -> Promise<Pudos> {
        
        let promise = NetworkManager.request(target: .dismissAll)
        
        return promise.then(execute: { (response) -> Promise<Pudos> in
            return Promise<Pudos>(value: try Pudos(json: response))
        })
        
    }
    
    
    func getRecentNotification() -> Promise<PudosNotification> {
        let promise = NetworkManager.request(target: .getRecentNotification)
        
        return promise.then(execute: { (response) -> Promise<PudosNotification> in
            return Promise<PudosNotification>(value: try PudosNotification(json: response))
        })
        
    }
    
    func getAllNotification() -> Promise<PudosNotification> {
        
        let promise = NetworkManager.request(target: .getAllNotification)
        
        return promise.then(execute: { (response) -> Promise<PudosNotification> in
            return Promise<PudosNotification>(value: try PudosNotification(json: response))
        })
    }

}

extension PudosService: PudosDriverOrderStore {
    
    
    func orderDetail(id: String) -> Promise<OrderDetailResponse> {
        let promise = NetworkManager.request(target: .orderDetail(id: id))
        return promise.then(execute: { (json) -> Promise<OrderDetailResponse>  in
            return Promise<OrderDetailResponse>(value: try OrderDetailResponse(json: json))
        })
    }
    
    func createOrder(wayPoints: String, instructions: String, reimb: Int, speed: Int, itemImages: [Data], promo: String?, optimize: Bool) -> Promise<PudosOrderCreationResponse> {
        
        
        let promise = NetworkManager.request(target: .createOrder(wayPoints: wayPoints, instructions: instructions, reimb: reimb, speed: speed, itemImages: itemImages, promo: promo, optimize: optimize))
        
        return promise.then(execute: { (response) -> Promise<PudosOrderCreationResponse> in
            
            return Promise<PudosOrderCreationResponse>(value: try PudosOrderCreationResponse(json: response))
        })
    }
    
    
    func cancelOrder(id: String) -> Promise<CancelPudosResponse> {
        let promise = NetworkManager.request(target: .cancelOrder(id: id))
        
        return promise.then(execute: { (response) -> Promise<CancelPudosResponse> in
            return Promise<CancelPudosResponse>(value: try CancelPudosResponse(json: response))
        })
    }
    
    
    func confirmOrder(method: String,orderId:String) -> Promise<OrderConfirmationResponse> {
        let promise = NetworkManager.request(target: .confirmOrder(method: method,orderId: orderId))
        
        return promise.then(execute: { (response) -> Promise<OrderConfirmationResponse> in
            return Promise<OrderConfirmationResponse>(value: try OrderConfirmationResponse(json: response))
        })
    }
    
    
    func findOrder(id: String?, status: String?, start: String?, end: String?, page: Int) -> Promise<PudosOrderResponse> {
        let promise = NetworkManager.request(target: .findOrder(id: id, status: status, start: status, end: end, page: page))
        
        return promise.then(execute: { (response) -> Promise<PudosOrderResponse> in
            return Promise<PudosOrderResponse>(value: try PudosOrderResponse(json: response))
        })
    }
    
   
}


