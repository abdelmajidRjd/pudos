//
//  NetworkManager.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import Moya
import PromiseKit
import SwiftyJSON
import Alamofire

struct NetworkManager {
    
    private enum Server {
        case mock, debug, release
    }
    
    
    typealias  Provider = MoyaProvider<PudosAPI>
    
    private static let server: Server = .debug
    
    @discardableResult
    static func request(target: PudosAPI) -> Promise<JSON> {
        
        let (promise, fulfill, reject) = Promise<JSON>.pending()
        
        let provider:Provider?
        
        
        switch server {
        case .mock:
            provider = Provider(stubClosure: MoyaProvider.delayedStub(0.8), plugins: target.plugins)
        case .debug:
            provider = Provider(plugins: target.plugins)
        case .release:
            provider = Provider(plugins: target.plugins)
        }
        
        
        
        provider!.request(target) { result in
            
            switch result {
            case .success(let response):

                saveCookie(response: response)
                
                let json = try? JSON(data: response.data)
                
                fulfill(json!)
                
            case .failure(let error):
                
                reject(error)
            }
            
        }
        
        return promise
        
    }
    
}

private func saveCookie(response: Response){
    
    if let cookies = HTTPCookieStorage.shared.cookies {
        guard let cookie = cookies.first else {return}
        SessionManager.sharedInstance.save(cookie: cookie)
        
        } else {
            if let
                headerFields = response.response?.allHeaderFields as? [String: String],
                let url = response.request?.url {
        
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
        
                if let first = cookies.first {
                    HTTPCookieStorage.shared.setCookie(first)
                    SessionManager.sharedInstance.save(cookie: first)
                }
        }
    }
}

