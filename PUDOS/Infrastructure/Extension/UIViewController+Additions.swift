



import UIKit
import SwiftyJSON

extension UIViewController {
    
    @objc class func instantiate() -> Self {
        fatalError("Requires implementation by subclasses")
    }
    
}
