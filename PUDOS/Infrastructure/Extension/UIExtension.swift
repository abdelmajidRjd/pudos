//
//  UIExtension.swift
//  Music_Recorder
//
//  Copyright © 2017 Rajad. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD

extension UIViewController {
    
    func showSpinner()  {
        
        let viewController = parent ?? self
        let hud = MBProgressHUD.showAdded(to: viewController.view, animated: true)
        hud.mode = .indeterminate
        
    }
    
    
    func hideSpinner() {
        
        let viewController = parent ?? self
        MBProgressHUD.hide(for: viewController.view, animated: true)
        
    }
    
    func show(error: Error, back: Bool? = false, close: Bool? = true) {
        
        // Read from 'errors.plist'
        let error = error as NSError
        let errorsPath = Bundle.main.path(forResource: "errors", ofType: "plist")!
        let errorsDictionary = (NSDictionary(contentsOfFile: errorsPath) as? [String: Any]) ?? [:]
        let errors = JSON(errorsDictionary)
        
        // Read description
        let description: String
        if let unwrappedDescription = errors[error.domain].dictionary?["\(error.code)"]?.string {
            
            description = unwrappedDescription
        } else if let _ = errors[error.domain].string {
            description = error.localizedDescription
        } else {
            description = "The application has encountered an error. "
        }
        
        
        // Show error
        let alertController = UIAlertController(title: "Oops!", message: description, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        let backAction = UIAlertAction(title: "Back", style: .destructive) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        if back! {
            alertController.addAction(backAction)
        }
        if close! {
            alertController.addAction(closeAction)
        }
        
        
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func show(error reason: String) {
        
        let alertController = UIAlertController(title: "App Error", message: reason, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        let backAction = UIAlertAction(title: "Back", style: .destructive) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
            alertController.addAction(backAction)
            alertController.addAction(closeAction)
            
            present(alertController, animated: true, completion: nil)
    }

    
}
// -MARK : COLLECTION VIEW EXTENSION
protocol ReuseableView: class {}

extension ReuseableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
// - MARK : Nib Extension
protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {
    static var NibName: String {
        return String(describing: self)
    }
}
// - MARK : Table View
extension UITableView {
    func register<T: UITableViewCell>(_: T) where T: ReuseableView, T: NibLoadableView{
        let nib = UINib(nibName: T.NibName, bundle: nil)
        self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}
extension UITableView {
    
    func dequeueReusableCell <T: UITableViewCell>(indexPath: IndexPath) -> T where T:ReuseableView {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier \(T.reuseIdentifier)")
        }
        return cell
    }
}
// MARK : - Collection View
extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T) where T: ReuseableView, T: NibLoadableView{
        let nib = UINib(nibName: T.NibName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
}
extension UICollectionView {
    
    func dequeueReusableCell <T: UICollectionViewCell>(indexPath: IndexPath) -> T where T:ReuseableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier \(T.reuseIdentifier)")
        }
        return cell
    }
}



