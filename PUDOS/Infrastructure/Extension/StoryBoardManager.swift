//
//  StoryBoardManager.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//
import UIKit

protocol StoryBoardProtocol {
    func instantiate(from name: StoryBoard) -> UIViewController
}
enum StoryBoard: String {
    case Main = "Main"
    case Authentication = "Auth"
    case Pudos = "Pudos"
}
struct StoryBoardManager<T>: StoryBoardProtocol where T: UIViewController {
    
    func instantiate(from name: StoryBoard) -> UIViewController {
        
        let identifier = String(describing: T.self)
        return UIStoryboard(name: name.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier) as! T
    }
}
