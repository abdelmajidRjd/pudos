//
//  Appearance.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class Appearance {
    
    static func setup() {
        
        // MARK: - UITabBar
        
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.backgroundColor = AppColor.main.tabbarToolbar
        
        // MARK: - button
        
        let buttonAppearance = UIButton.appearance()
        buttonAppearance.setTitleColor(AppColor.main.blue, for: .normal)
       
        
        // MARK: - Text Field
        let textFieldAppearance = UITextField.appearance()
        textFieldAppearance.backgroundColor = AppColor.main.background
        
        // MARK: - LABEL
       // let labelAppearance = UILabel.appearance()
        //labelAppearance.textColor = AppColor.main.blueLight
        
        
        
        
        
        
    }
    
}
