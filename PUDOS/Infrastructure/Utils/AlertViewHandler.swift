//
//  AlertViewHandler.swift
//  PUDOS
//
//  Created by Majid Inc on 29/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

struct AlertViewHandler {
    
    let contentStackView: UIStackView
    
    func showAlert(message: String,color: UIColor? = nil) {
        self.addAlertView()
        let errorView = contentStackView.arrangedSubviews[0] as! AlertView
        errorView.descriptionLabel.text = message
        if let color = color {
            errorView.backgroundColor = color
        } else {
            errorView.backgroundColor = AppColor.main.red
        }
        contentStackView.arrangedSubviews[1].isHidden = true
        contentStackView.arrangedSubviews[0].isHidden = false
        
    }
    
    func hideAlert(){
        
        contentStackView.arrangedSubviews[1].isHidden = false
        contentStackView.arrangedSubviews[0].isHidden = true
    }
    
    private func addAlertView(){
        
        let errorView = AlertView.instantiate()
        contentStackView.insertArrangedSubview(errorView, at: 0)
        contentStackView.arrangedSubviews[0].isHidden = true
    }
    
}

struct DateParser {
    
    let date: String
    private let data: [Substring]
    
    init(date: String) {
        self.date = date
        self.data = self.date.split(separator: " ")[0].split(separator: "-")
    }
    
    func day() -> String {
        return String(data[2])
    }
    
    func month() -> String{
        return  String(data[1])
    }
    
    func year()-> String {
        return  String(data[0])
    }
    func formattedDate() -> String {
        return data.joined(separator: "-")
    }
    
}

