
import Foundation

class Activity<T> {
    
    var onStart: (() -> ())?
    var onSuccess: ((T) -> ())?
    var onFailure: ((Error) -> ())?
    
}

class AnyActivity {
    
    var onStart: (() -> ())?
    var onSuccess: (() -> ())?
    var onFailure: ((Error) -> ())?
    
}
