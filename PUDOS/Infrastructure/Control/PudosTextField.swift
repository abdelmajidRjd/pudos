//
//  PudosTextField.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class PudosTextField: UIStackView, UITextFieldDelegate {
    
    
    lazy var leftImage: UIImageView = {self.viewWithTag(TAG.imageView.rawValue) as! UIImageView}()
    lazy var textField: UITextField = { self.viewWithTag(TAG.textField.rawValue) as! UITextField}()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textField.layer.masksToBounds = true
        textField.layer.borderColor = AppColor.main.contentSectionDivider.cgColor
        textField.layer.borderWidth = 0.5
        
        
        let color = AppColor.main.headBodySubhead
        self.textField.textColor = color
        self.textField.attributedPlaceholder = NSAttributedString(string: self.textField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : color])
        
    }
    
    override func awakeFromNib() {
        self.textField.delegate = self
    }
    
    var text: String? {
        get{
            return textField.text ?? ""
        } set{
            textField.text = newValue
        }
    }
    private var defaultSeparatorColor = UIColor.white
    // MARK: - Class Methods
    
    func show(state: Status){
        switch state {
        case .normal:
            leftImage.backgroundColor = AppColor.main.background
        case .error:
            leftImage.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        }
        
    }
    // MARK: - UITextFieldDelegate protocol methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        UIView.transition(with: leftImage, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.leftImage.backgroundColor = AppColor.main.contentSectionDivider
        }, completion: nil)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        UIView.transition(with: leftImage, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.leftImage.backgroundColor = AppColor.main.background
        }, completion: nil)
    }
    
    enum Status{
        case normal
        case error(NSError)
    }
    
    // MARK: - Associated Types
    private enum TAG: Int{
        case imageView = 1
        case textField = 2
        case error = 3
    }
    
}
