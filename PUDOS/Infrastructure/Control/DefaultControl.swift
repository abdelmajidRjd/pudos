//
//  DefaultControl.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class DefaultButton: UIButton {
   
    
   
    override func layoutSubviews() {
        let layer = self.layer
        //let height = self.frame.height
        layer.cornerRadius = 5
        
        if let imageView = self.imageView {
            self.imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
            
            self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: imageView.frame.width)
        }
        super.layoutSubviews()
    }
    
}
class CercleButton: UIButton {
    override func layoutSubviews() {
        let layer = self.layer
        let height = self.frame.height
        layer.cornerRadius = height / 2
        
        super.layoutSubviews()
    }
    
}
@IBDesignable
class RoundedButton: UIButton {
    @IBInspectable var borderWidth: CGFloat  = 5.0 {
        didSet{
            update()
        }
    }
    @IBInspectable var cornerRadius: CGFloat  = 5.0 {
        didSet{
            update()
        }
    }
    @IBInspectable var borderColor: UIColor  = .clear {
        didSet{
            update()
        }
    }
    
    override func layoutSubviews() {
        
        
        self.backgroundColor = UIColor.clear
        super.layoutSubviews()
    }
    
    fileprivate func update(){
        let layer = self.layer
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
    }
    
    
}
@IBDesignable
class RoundedLabel: UILabel {
    @IBInspectable var borderWidth: CGFloat  = 5.0 {
        didSet{
            update()
        }
    }
    @IBInspectable var cornerRadius: CGFloat  = 5.0 {
        didSet{
            update()
        }
    }
    @IBInspectable var borderColor: UIColor  = .clear {
        didSet{
            update()
        }
    }
    
    override func layoutSubviews() {
        
        
        self.backgroundColor = UIColor.clear
        super.layoutSubviews()
    }
    
    fileprivate func update(){
        let layer = self.layer
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
    }
    
    
}

// - MARK : Cercle Image
class CercleImage: UIImageView {
    
    override func layoutSubviews() {
        let layer = self.layer
        self.clipsToBounds = true
        let height = self.frame.height
        layer.cornerRadius = height / 2
        super.layoutSubviews()
    }
    
}

// - MARK : Default TextField
class DefaultTextField: UITextField{
    
   
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.insetBy(dx: 10, dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    
}
// - MARK : Default View

@IBDesignable
class DefaultView: UIView{
    
    override func layoutSubviews() {
        let layer = self.layer
        let shadowPath = UIBezierPath(rect: self.bounds)
        layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0.25)
        layer.shadowOpacity = 0.15
        layer.shadowPath = shadowPath.cgPath
        super.layoutSubviews()
    }
}


