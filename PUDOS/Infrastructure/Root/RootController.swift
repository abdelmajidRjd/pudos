//
//  RootController.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

struct RootController {
    
    static let `default` = RootController()
    private init(){}
    
    func initialeViewController(){
        
        
        if let _ = SessionManager.current, let cookie = SessionManager.cookie {
            HTTPCookieStorage.shared.setCookie(cookie)
            home()
        } else {
            auth()
        }

    }
    
    
     func home(){
        let homeTabViewController = StoryBoardManager<HomeTabBarViewController>().instantiate(from: .Pudos)
        
        switchRootViewController(rootViewController: homeTabViewController, animated: true, completion: nil)
        
       
       
    }
    
    private func auth(){
        
       let authNavigationViewController = StoryBoardManager<AuthNavigationViewController>().instantiate(from: .Authentication)
        
        switchRootViewController(rootViewController: authNavigationViewController, animated: true, completion: nil)
        
    }
    
    
}
extension RootController {
    
    private func switchRootViewController(rootViewController: UIViewController, animated: Bool, completion: (() -> Void)? ) {
        
        let window = UIApplication.shared.delegate?.window!
        window?.backgroundColor = UIColor.white
        if animated {
            UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(oldState)
                window!.rootViewController = rootViewController
                UIView.setAnimationsEnabled(oldState)
            }, completion: { (finished: Bool) -> () in
                if (completion != nil) {
                    completion!()
                }
            })
        } else {
            window?.rootViewController = rootViewController
        }
    }
    
}
