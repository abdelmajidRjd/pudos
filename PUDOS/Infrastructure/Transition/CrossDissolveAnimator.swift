//
//  CrossDissolveAnimator.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class CrossDissolveAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        setupTransition(using: transitionContext)
        // Access VCs
        
        let sourceVC = transitionContext.sourceViewController
        let destinationVC = transitionContext.destinationViewController
        
        // Animate
        let duration = self.transitionDuration(using: transitionContext)
        UIView.transition(from: sourceVC.view, to: destinationVC.view, duration: duration, options: .transitionCrossDissolve) { finished in
            transitionContext.completeTransition(true)
        }
        
    }
    
}
