//
//  TransitionExtension.swift
//  PUDOS
//
//  Created by Majid Inc on 28/01/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//
import UIKit

extension UIViewControllerAnimatedTransitioning {
    
    func setupTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let destinationVC = transitionContext.viewController(forKey: .to)
            else { return }
        
        destinationVC.view.frame = transitionContext.finalFrame(for: destinationVC)
        transitionContext.containerView.addSubview(destinationVC.view)
        
    }
    
}

extension UIViewControllerContextTransitioning {
    
    var destinationViewController: UIViewController {
        return viewController(forKey: .to)!
    }
    
    var sourceViewController: UIViewController {
        return viewController(forKey: .from)!
    }
    
}
