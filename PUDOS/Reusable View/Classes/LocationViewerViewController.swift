

import UIKit
import CoreLocation
import MapKit

class LocationViewerViewController: LocationViewController {
    
    // MARK: - Lifecycle
    
    override class func instantiate() -> LocationViewerViewController {
        let name = String(describing: LocationViewerViewController.self)
        return UIStoryboard(name: LocationViewController.storyboardName, bundle: nil)
            .instantiateViewController(withIdentifier: name) as! LocationViewerViewController
    }
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let coordinate = self.coordinate {
            placePin(at: coordinate)
        }
    }
    
    // MARK: - API
    
    func placePin(at coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
    }

}
