
//
//  PudosAnnotation.swift
//  PUDOS
//
//  Created by Majid Inc on 04/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import UIKit

class LocationSelectorSearchBar: UIView {
    
    @IBOutlet weak var textField: UITextField!

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if bounds.contains(point) {
            let point = self.convert(point, to: textField)
            let subview = textField.hitTest(point, with: event)
            return subview ?? textField
        } else {
            return nil
        }
    }
    
}
