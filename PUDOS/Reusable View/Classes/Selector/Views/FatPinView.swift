//
//  PudosAnnotation.swift
//  PUDOS
//
//  Created by Majid Inc on 04/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//
import UIKit

class FatPinView: UIView {
    
    // MARK: - Properties
    
    var color: UIColor? {
        didSet {
            pinImageView.tintColor = color
        }
    }
    
    private let pinImageView = UIImageView(image: #imageLiteral(resourceName: "pudos_pin"))
    
    lazy var ellipsisLayer: CAShapeLayer = { [unowned self] in
        
        let ellipseRect = CGRect(x: 0, y: 0, width: 10, height: 5)
        let ellipse = UIBezierPath(ovalIn: ellipseRect)
        
        let layer = CAShapeLayer()
        layer.bounds = ellipseRect
        layer.path = ellipse.cgPath
        layer.fillColor = UIColor.gray.cgColor
        layer.fillRule = kCAFillRuleNonZero
        layer.lineCap = kCALineCapButt
        layer.lineDashPattern = nil
        layer.lineDashPhase = 0.0
        layer.lineJoin = kCALineJoinMiter
        layer.lineWidth = 1.0
        layer.miterLimit = 10.0
        layer.strokeColor = UIColor.gray.cgColor
        
        return layer
        
        }()
    
    // MARK: - Super Properties
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: pinImageView.image?.size.width ?? 0, height: 69)
    }
    
    // MARK: - Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        // General
        self.isUserInteractionEnabled = false
        
        // Add shadow
        layer.addSublayer(ellipsisLayer)
        
        // Add pin image
        pinImageView.tintColor = color
        addSubview(pinImageView)
        
    }
    
    // MARK: - Draw
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Layout shadow
        ellipsisLayer.position.x = rect.midX
        ellipsisLayer.position.y = rect.maxY - ellipsisLayer.bounds.size.height + 2
        
    }
    
    // MARK: - Moving Pin
    
    func up() {
        
        // Animate pin image
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
            self.pinImageView.transform = CGAffineTransform(translationX: 0, y: -9)
        }, completion: nil)
        
        // Animate shadow
        ellipsisLayer.transform = CATransform3DMakeScale(0.5, 0.5, 1)
        
    }
    
    func down() {
        
        // Animate pin image
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
            self.pinImageView.transform = CGAffineTransform.identity
        }, completion: nil)
        
        // Animate shadow
        ellipsisLayer.transform = CATransform3DIdentity
        
    }
    
    // MARK: - Tint Color
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        pinImageView.tintColor = tintColor
    }
    
}
