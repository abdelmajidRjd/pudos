//
//  PudosAnnotation.swift
//  PUDOS
//
//  Created by Majid Inc on 04/02/2018.
//  Copyright © 2018 Rajad. All rights reserved.
//

import Foundation
import MapKit


class PudosAnnotation: NSObject,MKAnnotation {
    
    
    var id: UUID = UUID()
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var isSourceAddress: Bool
    private var state: State
    
    
    init(title: String, subTitle: String, coordinate: CLLocationCoordinate2D, source: Bool) {
        self.title = title
        self.subtitle = subTitle
        self.coordinate = coordinate
        self.isSourceAddress = source
        self.state = .unpin
    }
    
    func address() -> String? {
        return subtitle
    }
    
    
     func pin(){
        self.state = .pin
    }
    
     func unPin(){
        self.state = .unpin
    }
    
    func isSearchable() -> Bool {
        
        return self.state == .unpin
    }
    
    static func ==(lhs: PudosAnnotation, rhs: PudosAnnotation) -> Bool{
        return lhs.id == rhs.id
    }

    private enum State {
        case pin
        case unpin
    }
    
}


