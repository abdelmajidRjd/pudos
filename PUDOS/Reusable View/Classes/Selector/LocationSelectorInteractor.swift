

import Foundation
import SwiftyJSON
import PromiseKit


typealias Coordinate = (latitude: Double, longitude: Double, fullAddress: String)
protocol LocationSelectorInteractorInput {
    
    func search(_ text: String) -> Promise<[Coordinate]>
    func address(from lat: String,and lng: String) -> Promise<String>
    
}

class LocationSelectorInteractor: LocationSelectorInteractorInput {
    
    
    
    
    
    // MARK: - Properties
    
   //maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=AIzaSyAYqSUqJlGrK6DXkKENSMG9834D9h0eW20
    private var baseURL: String = "https://maps.googleapis.com/maps/api/geocode/json?"
    private var apiKey: String = "AIzaSyAYqSUqJlGrK6DXkKENSMG9834D9h0eW20"
    
    // MARK: - Lifecycle
    
    init() {
        
        guard let path = Bundle.main.path(forResource: "GoogleMaps-Info", ofType: "plist")
            
        else { return }
        let json = JSON(NSDictionary(contentsOfFile: path) as Any)
        
        baseURL = json["baseURL"].string ?? ""
        apiKey = json["apiKey"].string ?? ""
        
    }
    
    // MARK: - Input
    func address(from lat: String,and lng: String) -> Promise<String> {
        
        let (promise, fulfill, reject) = Promise<String>.pending()
        
        let urlString = "\(baseURL)latlng=\(lat),\(lng)&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        guard let url = URL(string: urlString) else {
            
            let error = NSError(domain: "LocationErrorDomain", code: 1, userInfo: nil)
            reject(error)
            return promise
        }
        
        // Search location
        DispatchQueue(label: "com.googlemaps.search").async {
            
            do {
                
                let data = try Data(contentsOf: url)
                let json = JSON(data)
                
                if let formattedAddress = json["results"][0]["formatted_address"].string
                    {
                    DispatchQueue.main.async(execute: {
                        fulfill(formattedAddress)
                    })
                }
                
            } catch let error {
                reject(error)
            }
            
        }
        
        return promise
        
    }
    
    
    func search(_ text: String) -> Promise<[Coordinate]> {
        
        // Prepare Promise
        let (promise, fulfill, reject) = Promise<[Coordinate]>.pending()
        var coodinates: [Coordinate] = []
        
        // Build URL
        let urlString = "\(baseURL)key=\(apiKey)&address=\(text)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        guard let url = URL(string: urlString) else {
            
            let error = NSError(domain: "LocationErrorDomain", code: 1, userInfo: nil)
            reject(error)
            return promise
        }
        
        // Search location
        DispatchQueue(label: "com.googlemaps.search").async {
            
            do {
                
                let data = try Data(contentsOf: url)
                let json = JSON(data)
                
                let resultCount = json["results"].count
                
                for result in 0..<resultCount {
                    
                    if let location = json["results"][result]["geometry"]["location"].dictionary,
                        let fullAddress = json["results"][result]["formatted_address"].string,
                        let latitude = location["lat"]?.double,
                        let longitude = location["lng"]?.double
                    {
                        
                        let coordinate = (latitude,longitude, fullAddress)
                        
                        coodinates.append(coordinate)
                    }
                    
                    
                }
                
                DispatchQueue.main.async(execute: {
                    fulfill(coodinates)
                })
                
                
                
            } catch let error {
                reject(error)
            }
            
        }
        
        return promise
        
    }
    
    // MARK: - Associated Types
    
    
    typealias Address = (address:String,coordinate: Coordinate)
    
}
