

import UIKit
import MapKit
import SwiftyJSON
import PromiseKit
import AddressBook


private let initPoint: CGFloat = 74.0
private let height: CGFloat = 64.0


typealias Address = (address: String,coordinate : Coordinate)

protocol LocationSelectorViewControllerDelegate: class {
    
    func locationSelectorViewController(_ viewController: LocationSelectorViewController, didFinishWithLocation location: CLLocation?)
 
}

protocol LocationSelectorActivityHandling {
    
    func handle(searchActivity activity: AnyActivity)
    
}


class LocationSelectorViewController: LocationViewController, UITextFieldDelegate {
    
    
    // MARK: - Dragging Behaviour
    @IBOutlet weak var mainDraggingView: UIView!
    fileprivate var initialePoint : CGPoint!
    fileprivate var coordinates: [Coordinate] = []
    @IBOutlet weak var addressTableView: UITableView!
    
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
   
    
    // MARK: - Markers
    private var sourceAnnotations : [PudosAnnotation]?
    private var destinationAnnotation: PudosAnnotation?
    // MARK: - Mapper Logic
    private var mapperManager : PudosMapBusinessLogic?
    
    // MARK: - Views
    
    @IBOutlet weak private var searchBar: LocationSelectorSearchBar!
    @IBOutlet weak private var fatPinView: FatPinView!
    @IBOutlet weak private var searchActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var startOrderButton: UIBarButtonItem!
    
    fileprivate var searchTextField: UITextField {
        return searchBar.textField
    }
    
    @IBAction func didTapShowItemButton(_ sender: Any) {
        
        self.performSegue(withIdentifier: "showItem", sender: nil)
    }
    
    
    @IBAction func didTapShowMyLocation(_ sender: Any) {
        
        if let location = locationManager.location?.coordinate {
            self.mapView.showsUserLocation = true
            centerMapOnCoordinate(coordinate: location)
            
        }
        
    }
    
    // MARK: - Pudos Source & Destination Annotation Manager
    private var sourceManager : SourceAnnotationManager?
    private var sourceAnnotation: PudosAnnotation?
    
    
    // MARK: - Fat Pin View Customization
    
    var fatPinViewColor: UIColor?
    
    // MARK: - Properties
    
    var interactor: LocationSelectorInteractorInput = LocationSelectorInteractor()
    
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation {
        let coordinate = mapView.region.center
        return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    // MARK: - Managing Delegate
    
    weak var delegate: LocationSelectorViewControllerDelegate?
    
    // MARK: - Init
    
    override class func instantiate() -> LocationSelectorViewController {
        return UIStoryboard(name: LocationViewController.storyboardName, bundle: nil)
            .instantiateInitialViewController() as! LocationSelectorViewController
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
    
        self.initialePoint = self.mainDraggingView.bounds.origin
        self.addressTableView.delegate = self
        self.addressTableView.dataSource = self
        self.addressTableView.register(AddressTableViewCell())
        
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = "New York"

        // Set the region to an associated map view's region
        request.region = mapView.region
        
        
        var region = mapView.region
        region.center = currentLocation.coordinate
        mapView.region = region
        
        // *** Annotation Gesture

        if let sourceCoordinates = locationManager.location?.coordinate {
            
        let sourceAnnotation = PudosAnnotation(title: "From", subTitle: "Where are we picking up the delivery from?", coordinate: sourceCoordinates, source: true)
            
            
         mapView.addAnnotation(sourceAnnotation)
            
         self.sourceAnnotations = [PudosAnnotation]()
         self.sourceAnnotations?.append(sourceAnnotation)
            
         let destinationAnnotation = PudosAnnotation(title: "To", subTitle: "", coordinate: sourceCoordinates,source: false)
            
        mapView.addAnnotation(destinationAnnotation)
        self.destinationAnnotation = destinationAnnotation
            
        self.mapperManager = PudosMapBusinessLogic(source: [sourceAnnotation], destination: self.destinationAnnotation!, mapView: mapView)
         
       self.sourceManager = SourceAnnotationManager(destination: destinationAnnotation)
         
        } 
        
            
    
 

        // Use default navigation bar style
        let navigationBar = navigationController?.navigationBar
        navigationBar?.setBackgroundImage(nil, for: .default)
        navigationBar?.shadowImage = nil
        navigationBar?.titleTextAttributes?[.foregroundColor] = AppColor.main.blue
        
        // Setup location manager
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if self.coordinate == nil {
            locationManager.startUpdatingLocation()
        }
        
        // Search Bar Customization
        searchBar.layer.shadowRadius = 0
        searchBar.layer.shadowOpacity = 1
        searchBar.layer.shadowOffset = CGSize(width: 0, height: 0.7)
        searchBar.layer.shadowColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1).cgColor
        
    }
    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Config
    
    func configure(with coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
    @IBAction func didTapMakeOrder(_ sender: Any) {
        
        if sourceManager!.isEligible() {
            if let _ = sourceAnnotation?.address() {
        self.performSegue(withIdentifier: "makeOrder", sender: nil)
            } else {
               self.show(error: "Please Fill & Drag a destination")
            }
        } else {
            self.show(error: "Please Drag and fill your source")
        }
        
    }
    
    
    @IBAction func didTapMyLocationButton(_ sender: Any) {
     
        guard let sourceCoordinates = locationManager.location?.coordinate else {
            self.show(error: "Please verify your location app permission")
            return}
        
        centerMapOnCoordinate(coordinate: sourceCoordinates)
        
        mapView.showsUserLocation = true
 
        
    }
    
    
    // MARK: - Pan Gesture Action
    
    
    @IBAction func didPanTray(_ sender: UIPanGestureRecognizer) {
        
    
        let lastPoint = self.view.bounds.height - 124
        self.initialePoint = CGPoint(x: 0, y: sender.location(in: view).y)
        if sender.state != .cancelled {
            
            if initialePoint.y < lastPoint && initialePoint.y > 100 {
            topLayoutConstraint.constant = initialePoint.y
            UIView.animate(withDuration: 0.01, animations: {
                self.view.layoutIfNeeded()
            })
            }
            
        }
        
        if sender.state == .ended {
        
        let translation = sender.translation(in: mainDraggingView)
        print(translation.y)
        if translation.y > 20 {
            topLayoutConstraint.constant = lastPoint
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 2, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
        
        if translation.y < -20 {
            topLayoutConstraint.constant = 100
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 2, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
        }
        
        
        
    }
    
    
    
    
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier else {return}
        
        switch identifier {
            
            case "makeOrder":
                let destination = segue.destination as! OrderNavViewController
                destination.sourceManager = sourceManager
                destination.dest = destinationAnnotation
            
            
            
            case "showItem":
                
                let destination = segue.destination as! ItemsViewController
                destination.popoverPresentationController?.delegate = self
                destination.sourceAnnotation = sourceAnnotation
                destination.delegate = self
            
            
            break
  
        default:
            break
            
        }
        
        
        
    }
    // MARK: - Create Annotations
    
    @IBAction func didTapcreateSoureAnnotation(_ sender: Any) {
        
        guard let sourceCoordinates = locationManager.location?.coordinate else {
            self.show(error: "Please verify your location app permission")
            return}
        
        centerMapOnCoordinate(coordinate: sourceCoordinates)
        
        let annotation = PudosAnnotation(title: "New Source", subTitle: "From where we pick up ?", coordinate: sourceCoordinates, source: true)
        
        mapView.addAnnotation(annotation)
        
        
    }
    
    
    // MARK: - User Interaction
    
    @IBAction func tapGestureHandler(_ sender: UITapGestureRecognizer) {
        
        if searchTextField.isFirstResponder {
            view.endEditing(true)
        }
        
    }

    // MARK: - Networking
    
    private func search(_ text: String) {
        
        let activity = AnyActivity()
        handle(searchActivity: activity)
        activity.onStart?()
        
        interactor.search(text).then { coordinates -> Void in
            
            self.coordinates = coordinates
            self.addressTableView.reloadData()
            
            activity.onSuccess?()
            let locationCoordinate = CLLocationCoordinate2D(latitude: coordinates.first!.latitude, longitude: coordinates.first!.longitude)
            self.centerMapOnCoordinate(coordinate: locationCoordinate)
            
            
            
            guard let mapper = self.mapperManager else {
                return
            }
            
            mapper.changeCoordinate(coordinate: locationCoordinate)
            
  
            /*
            if let source = self.sourceAnnotation,
                let destination = self.destinationAnnotation {
                self.mapView.removeAnnotations([source,destination])
                
                self.sourceAnnotation?.coordinate = coordinate
                self.destinationAnnotation?.coordinate = coordinate
                self.mapView.addAnnotation(self.sourceAnnotation!)
                self.mapView.addAnnotation(self.destinationAnnotation!)
 */
                
            
            
      
        }.catch { error in
            
            activity.onFailure?(error)
        }
        
    }
 
    // MARK: - Search Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            search(text)
        }
        textField.resignFirstResponder()
        return true
    }
    
}

extension LocationSelectorViewController: LocationSelectorActivityHandling {
    
    func handle(searchActivity activity: AnyActivity) {
        
        activity.onStart = { [unowned self] in
            self.searchActivityIndicator.startAnimating()
        }
        
        activity.onSuccess = { [unowned self] in
            self.searchActivityIndicator.stopAnimating()
        }
        
        activity.onFailure = { [unowned activity] _ in activity.onSuccess?() }
        
    }
    
}

extension LocationSelectorViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension LocationSelectorViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else { return }
        
        centerMapOnCoordinate(coordinate: location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002))
        manager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    
}

extension LocationSelectorViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        //fatPinView.up()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        //fatPinView.down()
    }
    
    
    
   
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        
        
        
        switch newState {
            case .starting:
                view.dragState = .dragging
            
            case .ending, .canceling:
                
                mapView.showsUserLocation = false 
                let coordinate = view.annotation?.coordinate
                let pin = view.annotation as! PudosAnnotation
                
                
                
                if let lat = coordinate?.latitude,
                    let lng = coordinate?.longitude {
                    
                    interactor.address(from: lat.description, and: lng.description).then(execute: { (address) -> Void in
                        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                        
                        if pin.isSourceAddress {
                            
                            pin.coordinate = coordinate
                            self.sourceAnnotation = pin
                            pin.title = String(address.split(separator: ",")[1])
                            pin.subtitle = address
                            view.annotation = pin  
                            
                            self.performSegue(withIdentifier: "showItem", sender: nil)
                            
                            
                            
                        } else {
                            
                            pin.coordinate = coordinate
                            pin.title = String(address.split(separator: ",")[1])
                            pin.subtitle = address
                            view.annotation = pin
                        
                            
                        }
                        
                        
                    })
                    
                }
            // show Pop over
            
            
            default: break
        }
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        
        let placeMark = MKPlacemark(coordinate: view.annotation!.coordinate )
        
        let mapItem = MKMapItem(placemark: placeMark)
        mapItem.openInMaps(launchOptions: launchOptions)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseIdentifier = "pin"
        if annotation is MKUserLocation {
            return nil
        }
        guard let pudosAnnotation = annotation as? PudosAnnotation else {
            return nil
        }
        
        if #available(iOS 11.0, *) {
            
            let marker = MKMarkerAnnotationView(annotation: pudosAnnotation, reuseIdentifier: reuseIdentifier)
            if pudosAnnotation.isSourceAddress {
                marker.glyphImage = #imageLiteral(resourceName: "ic_location_on_36pt")
                marker.markerTintColor = #colorLiteral(red: 0.2489523208, green: 0.2609169371, blue: 0.4912123197, alpha: 1)
                
            } else {
                marker.glyphImage = #imageLiteral(resourceName: "ic_flag")
                marker.markerTintColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            }
            marker.subtitleVisibility = .visible
            marker.titleVisibility = .visible
            
            marker.isDraggable = true
            marker.canShowCallout = true
            marker.animatesWhenAdded = true

            
            return marker
            
            
            
        } else {
            
            let pin = MKPinAnnotationView(annotation: pudosAnnotation, reuseIdentifier: reuseIdentifier)
            if pudosAnnotation.isSourceAddress {
                pin.pinTintColor = #colorLiteral(red: 0.2489523208, green: 0.2609169371, blue: 0.4912123197, alpha: 1)
            } else {
                pin.pinTintColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            }
            
            pin.isDraggable = true
            pin.canShowCallout = true
            pin.animatesDrop = true 
            
            
            
            
            
            return pin 
        }
        
        
    }
    
    
  
}

extension LocationSelectorViewController : UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension LocationSelectorViewController: ItemSaveOrderProtocol {
    
    func save(order: ORDER) {
        
        guard let sourceAnnotation = self.sourceAnnotation else {return}
        
        sourceAnnotation.pin()
        
        self.sourceManager!.add(annotation: sourceAnnotation, order: order)
    }
    

}
extension LocationSelectorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coordinates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as AddressTableViewCell
        
        let coordinate = coordinates[indexPath.row]
        
        cell.bind(coordinate)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AddressTableViewCell
        let coordinate = cell.coordinate
        let locationCoordinate = CLLocationCoordinate2D(latitude: coordinate!.latitude, longitude: coordinate!.longitude)
        self.centerMapOnCoordinate(coordinate: locationCoordinate)
        
            let lastPoint = self.view.bounds.height - 64
            topLayoutConstraint.constant = lastPoint
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    
}



// MARK: - Pan Gesture

