

import UIKit
import CoreLocation
import MapKit


class LocationViewController: UIViewController {

    // MARK: - Properties
    
    var coordinate: CLLocationCoordinate2D!
    
    // MARK: - Constants
    
    static let storyboardName = "Location"
    
    // MARK: - Views
    
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        guard let coordinate = coordinate else { return }
        centerMapOnCoordinate(coordinate: coordinate, span: span)
        
    }
    
    // MARK: - User Interaction
    
    @IBAction func didClickCloseButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - API

    func centerMapOnCoordinate(coordinate: CLLocationCoordinate2D, span: MKCoordinateSpan? = nil) {
        
        // Create region
        var region = mapView.region
        if let span = span {
            region.span = span
        }
        region.center = coordinate
        
        // Center
        mapView.setRegion(region, animated: true)
        
    }
    // MARK: - Configuration
    
   
}
